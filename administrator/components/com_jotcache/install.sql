-- version $Id: install.sql,v 1.1 2011/04/07 12:33:31 Vlado Exp $
-- package JotCache
-- copyright (C) 2010-@curyear@ Vladimir Kanich
-- license http://www.gnu.org/copyleft/gpl.html GNU/GPL
CREATE TABLE IF NOT EXISTS `#__jotcache` (
        `fname` varchar(76) NOT NULL,
        `com` varchar(100) NOT NULL,
        `view` varchar(100) NOT NULL,
        `id` int(11) NOT NULL,
        `ftime` datetime NOT NULL,
 	`mark` tinyint(1) NOT NULL,
        `checked_out` int(11) NOT NULL,
        PRIMARY KEY  (`fname`));
CREATE TABLE IF NOT EXISTS `#__jotcache_exclude` (
        `id` int(11) NOT NULL auto_increment,
        `name` varchar(64) NOT NULL default '',
        `value` text NOT NULL,
        PRIMARY KEY  (`id`));