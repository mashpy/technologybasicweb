<?php
/*
 * @version $Id: install.php,v 1.1 2011/04/07 12:33:31 Vlado Exp $
 * @package JotCache
 * @copyright (C) 2010-2011 Vladimir Kanich
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
  jimport('joomla.installer.installer');
$installer = new JInstaller();
$installer->install($this->parent->getPath('source'). '/plugin');
echo "<p>Installation of JotCache ver.1.3.0 successful!</p><p> More information you can find in JotCache Help (Components-&gt;JotCache).</p><p>Home site : <a href=\"http://www.kanich.net/radio/site\" target=\"_blank\">http://www.kanich.net/radio/site</a>.<br><br>When necessary contact me using <a href=\"http://www.kanich.net/radio/site/contact\" target=\"_blank\">http://www.kanich.net/radio/site/contact</a> with any questions.</p>";
return true;
?>