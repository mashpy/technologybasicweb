<?php
/*
 * @version $Id: toolbar.php,v 1.2 2011/04/06 12:40:05 Vlado Exp $
 * @package JotCache
 * @copyright (C) 2010-2011 Vladimir Kanich
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
class JotcacheToolbar extends JToolBarHelper {
public static function title($title) {
$app = JFactory::getApplication();
$site = JURI::base();
    $html = '<img border="0" width="100" height="54" src="' . $site
. 'components/com_jotcache/images/jotcache-logo-2.gif" alt="JotCache Logo" align="left">';
$html .= '<div class="header">&nbsp;' . $title;
$html .= "</div>\n";
$app->set('JComponentTitle', $html);
}}?>
