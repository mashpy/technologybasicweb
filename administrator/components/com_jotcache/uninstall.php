<?php
/*
 * @version $Id: uninstall.php,v 1.1 2011/04/07 12:33:31 Vlado Exp $
 * @package JotCache
 * @copyright (C) 2010-2011 Vladimir Kanich
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */
function com_uninstall() {
$database = &JFactory::getDBO();
$query = "DELETE FROM #__extensions where `element`='com_jotcache'";
$database->setQuery($query);
if (!$database->query())
JError::raiseNotice(100, $database->getErrorMsg());
$query = "DROP TABLE #__jotcache";
$database->setQuery($query);
if (!$database->query())
JError::raiseNotice(100, $database->getErrorMsg());
$query = "DROP TABLE #__jotcache_exclude";
$database->setQuery($query);
if (!$database->query())
JError::raiseNotice(100, $database->getErrorMsg());
if (count(JError::getErrors()) > 0) {
echo "Error condition - Uninstallation not successfull! You have to manually remove com_jotcache from '.._components' table as well as to drop '.._jotcache' tabel";
} else {
echo "Uninstallation successfull!";
}}?>