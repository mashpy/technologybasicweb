<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12193:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="ওয়েবসাইট, অ্যাফিলিয়েট, বা, এ, লিংক, মার্কেটিং, কোন, কোম্পানি, দিয়ে, প্রোগ্রাম, তাদের, amazon.com, যা, কমিশন, একটি" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Affiliate - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Affiliate - internet ওয়েবসাইট অ্যাফিলিয়েট হচ্ছে এমন একটি প্রোগ্রাম যা দিয়ে ইণ্টারনেট বাজার পরিচালনা করা হয়। ১৯৯০ সালে দিকে ওয়েবসাইট অ্যাফিলিয়েট প্রোগ্রাম চালু হয় যা ইন্টারনেট মার্কেটিং কে নতুন উচ্চতায় নিয়ে যায়। অ্যাফিলিয়েট প্রোগ্রাম এ কোন একটি কোম্পানি তা" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Affiliate - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Affiliate						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by পাভেল ফাহমি				</dd>
	
		<dd class="hits">
		Hits: 80		</dd>
 </dl>

	
	<p><span class="text_exposed_show"><strong> </strong>ওয়েবসাইট অ্যাফিলিয়েট   হচ্ছে এমন একটি প্রোগ্রাম যা দিয়ে ইন্টারনেট বাজার  পরিচালনা  করা হয়। ১৯৯০  সালে দিকে </span><span class="text_exposed_show">ওয়েবসাইট</span><span class="text_exposed_show"> </span><span class="text_exposed_show">অ্যাফিলিয়েট</span><span class="text_exposed_show"> প্রোগ্রাম চালু হয় যা ইন্টারনেট মার্কেটিং কে নতুন উচ্চতায় নিয়ে যায়। </span><span class="text_exposed_show">অ্যাফিলিয়েট</span><span class="text_exposed_show"> </span><span class="text_exposed_show">প্রোগ্রাম </span><span class="text_exposed_show">এ কোন একটি   কোম্পানি তার  পণ্যের বিজ্ঞাপন <span style="text-decoration: underline;"><a href="internet/link">লিংক</a></span> বা  ব্যানার আকারে অন্য একটি  ওয়েবসাইট এ  দিয়ে থাকে। এই লিংক  বা ব্যানারের মাধ্যমে মাধ্যমে ঐ কোম্পানির  পণ্যের যে  পরিমান বিক্রয় হয়, তার  ভিত্তিতে কোম্পানি ঐ ওয়েবসাইট কে কমিশন  দিয়ে থাকে।  কিন্তু এই কমিশন অধিকাংশ  ক্ষেত্রে ৫% এর কম, যা খুবই নগণ্য।  উদাহরণসরূপঃ  কেউ amazon.com কোন ওয়েবসাইটে তাদের বিজ্ঞাপন দিতে চায়। </span><span class="text_exposed_show">এখন </span><span class="text_exposed_show">amazon.com-র</span><span class="text_exposed_show"> </span><span class="text_exposed_show">লিংক  বা <span style="text-decoration: underline;"> <a href="internet/banner-ad">ব্যানার </a></span>সেই ওয়েবসাইটটি তাদের পেজে দিল। এখন যদি কোন ভিজিটর amazon.com এর লিংক বা  ব্যানারে ক্লিক করে এবং  পণ্য ক্রয় করে, তবে   amazon.com  তাদের বিক্রয় থেকে উক্ত ওয়েবসাইটটিকে কমিশন দেবে। </span><span class="text_exposed_show">ওয়েবসাইট </span><span class="text_exposed_show">অ্যাফিলিয়েট</span><span class="text_exposed_show"> এ   বিজ্ঞাপনদাতা কোম্পানিগুলো বেশি লাভবান হয় । তবে </span><span class="text_exposed_show">ওয়েবসাইট</span><span class="text_exposed_show"> </span><span class="text_exposed_show">অ্যাফিলিয়েট</span><span class="text_exposed_show"> তৈরী   করতে এবং পরিচালনা করতে অনেক পরিশ্রমের পাশাপাশি অনেক টাকার ও প্রয়োজন হয়।   তাই ছোট ছোট কোম্পানি গুলো এ থেকে খুব বেশি লাভবান হতে পারে না। এরপরও প্র</span><span class="text_exposed_show">যুক্তি</span><span class="text_exposed_show"> ও   মার্কেট নিয়ে  গবেযণকারী প্রতিষ্ঠান</span><span class="text_exposed_show"> ফরেস্টার রিসার্চ-র মতে, </span><span class="text_exposed_show">অ্যাফিলিয়েট</span><span class="text_exposed_show"> মার্কেটিং</span><span class="text_exposed_show"> হচ্ছে এখন পর্যন্ত সবচেয়ে কার্যকরী অনলাইন </span><span class="text_exposed_show">মার্কেটিং</span><span class="text_exposed_show"> পদ্ধতি।</span><span class="text_exposed_show"><strong> </strong></span></p>
<p><span class="text_exposed_show"><strong><img src="images/internet/AE/affiliate.jpg" border="0" /><br /></strong></span></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/ajax" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/adware" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/affiliate" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/affiliate" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:63:"Affiliate - internet - টেকনোলজি বেসিক";s:11:"description";s:653:"Affiliate - internet ওয়েবসাইট অ্যাফিলিয়েট হচ্ছে এমন একটি প্রোগ্রাম যা দিয়ে ইণ্টারনেট বাজার পরিচালনা করা হয়। ১৯৯০ সালে দিকে ওয়েবসাইট অ্যাফিলিয়েট প্রোগ্রাম চালু হয় যা ইন্টারনেট মার্কেটিং কে নতুন উচ্চতায় নিয়ে যায়। অ্যাফিলিয়েট প্রোগ্রাম এ কোন একটি কোম্পানি তা";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:263:"ওয়েবসাইট, অ্যাফিলিয়েট, বা, এ, লিংক, মার্কেটিং, কোন, কোম্পানি, দিয়ে, প্রোগ্রাম, তাদের, amazon.com, যা, কমিশন, একটি";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:63:"Affiliate - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}