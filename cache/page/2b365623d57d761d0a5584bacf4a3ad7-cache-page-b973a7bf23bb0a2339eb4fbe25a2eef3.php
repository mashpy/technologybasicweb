<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:13157:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="অ্যাক্সেস, পয়েন্ট, হয়।, ওয়্যারলেস, যুক্ত, দেওয়া, থাকে।, রাউটার, করা, নেটওয়ার্কে, এর, সাথে, বানানো, হিসেবে, অফিসে" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Access Point - hardware - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Access Point - hardware হল এমন একটি ব্যাবস্থা যার মাধ্যমে কোন ওয়্যারলেস ডিভাইস কে নেটওয়ার্ক এ যুক্ত করা যায়। বেশীরভাগ ক্ষেত্রে অ্যাক্সেস পয়েন্টে বিল্ট-ইন p রাউটার থাকে এবং সেগুলো নেটওয়ার্কে যুক্ত থাকে। নয়ত অ্যাক্সেস পয়েন্টকে মডেম অথবা সুইচের সাথে তার দিয়ে" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Access Point - hardware - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104" class="current active"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	টেকনোলজি বেসিক - হার্ডওয়্যার টার্ম</h1>
		<h2>
										Access Point						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by Super User				</dd>
	
		<dd class="hits">
		Hits: 73		</dd>
 </dl>

	
	<p style="margin-top: 0.19in; margin-bottom: 0.19in;"><span><span><span lang="bn-IN">হল এমন একটি</span></span><span> </span><span><span lang="bn-IN">ব্যাবস্থা যার মাধ্যমে কোন ওয়্যারলেস ডিভাইস কে নেটওয়ার্ক এ যুক্ত করা যায়</span></span><span><span lang="hi-IN">।</span></span><span> </span><span><span lang="bn-IN">বেশীরভাগ ক্ষেত্রে অ্যাক্সেস পয়েন্টে বিল্ট</span></span></span>-<span><span><span lang="bn-IN">ইন </span></span></span> p { margin-bottom: 0.08in; }<span><span><span lang="bn-IN">রাউটার থাকে এবং সেগুলো</span></span><span> </span><span><span lang="bn-IN">নেটওয়ার্কে যুক্ত থাকে</span></span><span><span lang="hi-IN">। </span></span><span><span lang="bn-IN">নয়ত অ্যাক্সেস পয়েন্টকে মডেম অথবা সুইচের সাথে তার</span></span><span> </span><span><span lang="bn-IN">দিয়ে সংযোগ দেওয়া থাকে</span></span><span><span lang="hi-IN">।</span></span></span><span><span><span lang="bn-IN">অধিকাংশ বাসা</span></span></span>-<span><span><span lang="bn-IN">বাড়ি</span></span></span>, <span><span><span lang="bn-IN">অফিসে অ্যাক্সে</span></span></span>...<span><span><span lang="bn-IN">স পয়েন্ট হিসেবে ওয়্যারলেস রাউটার থাকে</span></span></span>, <span><span><span lang="bn-IN">আর সেই</span></span><span> </span><span><span lang="bn-IN">রাউটার গুলো ডিএসএল বা তারযুক্ত মডেমের সাথে লাগানো থাকে</span></span><span><span lang="hi-IN">। </span></span><span><span lang="bn-IN">বড়সর এলাকায়</span></span><span> </span><span><span lang="bn-IN">একসাথে অনেকগুলো অ্যাক্সেস পয়েন্ট ব্যবহার করা হয় যেন অনেক দূর থেকেও সহজেই</span></span><span> </span><span><span lang="bn-IN">নেটওয়ার্কে যুক্ত হওয়া সম্ভব হয়</span></span><span><span lang="hi-IN">। </span></span><span><span lang="bn-IN">কিছু অ্যাক্সেস পয়েন্ট আছে যেগুলো</span></span><span> </span><span><span lang="bn-IN">জনসাধারণের জন্য উন্মুক্ত</span></span><span><span lang="hi-IN">। </span></span><span><span lang="bn-IN">এগুলো বিভিন্ন দোকান</span></span></span>, <span><span><span lang="bn-IN">ক্যাফে</span></span></span>, <span><span><span lang="bn-IN">রেস্ট্যুরেন্টে</span></span><span> </span><span><span lang="bn-IN">পাওয়া যায়</span></span><span><span lang="hi-IN">। </span></span><span><span lang="bn-IN">আবার সীমাবদ্ধ নেটওয়ার্ক গডে তুলতেও অ্যাক্সেস পয়েন্ট বানানো</span></span><span> </span><span><span lang="bn-IN">হয়</span></span><span><span lang="hi-IN">। </span></span><span><span lang="bn-IN">যেমন একটি অফিসে শুধু কর্মীদের জন্য নিরাপদ ও বিশেষ অ্যাক্সেস পয়েন্ট</span></span><span> </span><span><span lang="bn-IN">বানানো হয়</span></span><span><span lang="hi-IN">।</span></span></span></p>
<p style="margin-top: 0.19in; margin-bottom: 0.19in;"><span><span><span lang="bn-IN">অ্যাক্সেস পয়েন্ট হিসেবে সাধারণতঃ ওয়্যারলেস মাধ্যম ব্যবহার করা হয়</span></span><span><span lang="hi-IN">। </span></span><span><span lang="bn-IN">তবে</span></span><span> </span><span><span lang="bn-IN">ওয়াই</span></span></span>-<span><span><span lang="bn-IN">ফাই</span></span></span>, <span><span><span lang="bn-IN">ব্লু</span></span></span>-<span><span><span lang="bn-IN">টুথ ইত্যাদি এর মাধ্যমেও সংযোগ দেওয়া হয়</span></span><span><span lang="hi-IN">। </span></span><span><span lang="bn-IN">বেশীরভাগ</span></span><span> </span><span><span lang="bn-IN">অ্যাক্সেস পয়েন্টের মাধ্যমে গ্রাহককে ইন্টারনেট সুবিধা দেওয়া হয়ে থাকে</span></span><span><span lang="hi-IN">।</span></span></span></p>
<p style="margin-top: 0.19in; margin-bottom: 0.19in;"> </p>
<p style="margin-top: 0.19in; margin-bottom: 0.19in;"><span><span><span lang="bn-IN">ওয়াই</span></span></span>-<span><span><span lang="bn-IN">ফাই ডিভাইস এর ক্ষেত্রে অ্যাক্সেস পয়েন্ট কে </span></span></span>WAP <span><span><span lang="bn-IN">বা </span></span></span>Wireless Access Point <span><span><span lang="bn-IN">বলা হয়</span></span><span><span lang="hi-IN">।</span></span></span></p>
<p> </p>
<p> </p> 
				<ul class="pagenav">
					<li class="pagenav-next">
						<a href="/hardware/active-matrix" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://technologybasic.com/hardware/access-point" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://technologybasic.com/hardware/access-point" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="104" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:66:"Access Point - hardware - টেকনোলজি বেসিক";s:11:"description";s:641:"Access Point - hardware হল এমন একটি ব্যাবস্থা যার মাধ্যমে কোন ওয়্যারলেস ডিভাইস কে নেটওয়ার্ক এ যুক্ত করা যায়। বেশীরভাগ ক্ষেত্রে অ্যাক্সেস পয়েন্টে বিল্ট-ইন p রাউটার থাকে এবং সেগুলো নেটওয়ার্কে যুক্ত থাকে। নয়ত অ্যাক্সেস পয়েন্টকে মডেম অথবা সুইচের সাথে তার দিয়ে";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:280:"অ্যাক্সেস, পয়েন্ট, হয়।, ওয়্যারলেস, যুক্ত, দেওয়া, থাকে।, রাউটার, করা, নেটওয়ার্কে, এর, সাথে, বানানো, হিসেবে, অফিসে";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:66:"Access Point - hardware - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}