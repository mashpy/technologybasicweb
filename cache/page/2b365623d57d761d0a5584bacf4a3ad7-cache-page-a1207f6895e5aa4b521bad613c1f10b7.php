<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11074:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="করতে, আপনার, আপনি, সাথে, ব্যক্তিগত, বন্ধুদের, যোগাযোগের, নেটওয়ার্কিং, শেয়ার, সাইট, তথ্য, হয়ে, শুরু, যুক্ত, খোলার" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Social Networking - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Social Networking - internet ১৯৯০ সালের দিকে যখন ওয়েব জনপ্রিয় হয়ে উঠে, তখন থেকে মানুষ তার ব্যক্তিগত তথ্" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Social Networking - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Social Networking						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by পাভেল ফাহমি				</dd>
	
		<dd class="hits">
		Hits: 33		</dd>
 </dl>

	
	<p>১৯৯০ সালের দিকে যখন ওয়েব জনপ্রিয় হয়ে উঠে, তখন থেকে মানুষ তার ব্যক্তিগত তথ্য ইন্টারনেটে শেয়ার করতে শুরু করে। কিন্তু তখন তথ্য শেয়ারের ক্ষেত্রে নানা সীমাবদ্বতা ছিল। এরপর ইন্টারনেট ইউজাররা নিজস্ব হোমপেজ খুলে সেখানে তথ্য শেয়ার করতে শুরু করলো। কিন্তু যেহেতু এগুলো ছিল ব্যক্তিগত সাইট তাই এখানেও ছিল সামজিকতার অভাব। ২০০০ সালের দিকে সোস্যাল নেটওয়ার্কিং সাইট আবিষ্কারের সাথে সাথে এসব সীমাবদ্বতা দূর হয়ে গেল। সোস্যাল নেটওয়ার্কিং সাইটের মাধমে এখন মানুষ খুব সহজে পরস্পরের সাথে যোগাযোগ করতে পারে। বর্তমান সময়ের সবচেয়ে জনপ্রিয় দুইটি সোস্যাল নেটওয়ার্কিং সাইট হছে ফেসবুক,টুইটার। সামাজিক যোগাযোগের সাইটগুলো আপনাকে একটি অ্যাকাউন্ট খোলার সুযোগ দেয়। যেখানে আপনি আপনার ব্যক্তিগত তথ্যের পাশাপাশি আপনার ছবিও যুক্ত করতে পারেন। অ্যাকাউন্ট খোলার পর আপনি আপনার বন্ধুদের অ্যাড করতে পারেন। এরপর আপনি আপনার ব্যক্তিগত অনূভুতি,ছবি,ভিডিও ইত্যাদি বন্ধুদের সাথে শেয়ার করতে পারেন। আপনি আপনার বন্ধুকে মেসেজ পাঠাতে,বন্ধুর সাথে চ্যাট করতে এমনকি বন্ধুর প্রোফাইলে কমেন্টও করতে পারেন। এছাড়া আপনি আপনার বন্ধুদের নিয়ে আলাদা গ্রুপ করতে পারেন। এছাড়াও রয়েছে আরও অসংখ্য ফিচার। খুব শীঘ্রই সামাজিক যোগাযোগের সাইটগুলোতে ভিডিও চ্যাটের সুবিধা যুক্ত হচ্ছে। সামাজিক যোগাযোগের সাইটগুলোকে ধন্যবাদ দিতে হয় কারণ এর মাধ্যমে আমরা আমদের স্কুল কলেজ জীবনে হারানো বন্ধুদের সহজ়ে খুজে পাচ্ছি। এককথায় বলা যায় সামাজিক যোগাযোগের সাইটগুলো ইন্টারনেট ব্যবহারকারীদের মধ্যে একটি ভার্চুয়াল সমাজ তৈরী করেছে।<br /><img src="images/internet/PT/social-networking.jpg" border="0" /></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/spam" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/soap" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://technologybasic.com/internet/social-networking" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://technologybasic.com/internet/social-networking" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:71:"Social Networking - internet - টেকনোলজি বেসিক";s:11:"description";s:223:"Social Networking - internet ১৯৯০ সালের দিকে যখন ওয়েব জনপ্রিয় হয়ে উঠে, তখন থেকে মানুষ তার ব্যক্তিগত তথ্";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:280:"করতে, আপনার, আপনি, সাথে, ব্যক্তিগত, বন্ধুদের, যোগাযোগের, নেটওয়ার্কিং, শেয়ার, সাইট, তথ্য, হয়ে, শুরু, যুক্ত, খোলার";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:71:"Social Networking - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}