<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:20012:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="software" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="software - টেকনোলজি বেসিক" />
  <meta name="author" content="" />
  <meta name="description" content="software" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>software - টেকনোলজি বেসিক</title>
  <link href="/software?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
  <link href="/software?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>
  <script type="text/javascript">

		window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});function keepAlive() {	var myAjax = new Request({method: "get", url: "index.php"}).send();} window.addEvent("domready", function(){ keepAlive.periodical(840000); });
  </script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147" class="current active"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="category-list">

		<h1>
		টেকনোলজি বেসিক - সফটওয়্যার টার্ম	</h1>
	
	
	
	<div class="cat-items">
		

<form action="http://www.technologybasic.com/software" method="post" name="adminForm" id="adminForm">
	
				<div class="display-limit">
			Display #&#160;
			<select id="limit" name="limit" class="inputbox" size="1" onchange="this.form.submit()">
	<option value="5">5</option>
	<option value="10">10</option>
	<option value="15">15</option>
	<option value="20">20</option>
	<option value="25">25</option>
	<option value="30">30</option>
	<option value="50">50</option>
	<option value="100">100</option>
	<option value="0" selected="selected">All</option>
</select>
		</div>
		
	
	<table class="category">
				<thead>
			<tr>
				<th class="list-title" id="tableOrdering">
					<a href="javascript:tableOrdering('a.title','asc','');" title="Click to sort by this column">Title</a>				</th>

				
				
								<th class="list-hits" id="tableOrdering4">
					<a href="javascript:tableOrdering('a.hits','asc','');" title="Click to sort by this column">Hits</a>				</th>
							</tr>
		</thead>
		
		<tbody>

									<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/abend">
							Abend</a>

											</td>

					
					
										<td class="list-hits">
						48					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/access">
							Access</a>

											</td>

					
					
										<td class="list-hits">
						47					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/acl">
							ACL</a>

											</td>

					
					
										<td class="list-hits">
						42					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/add-on">
							Add-on</a>

											</td>

					
					
										<td class="list-hits">
						52					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/aix">
							AIX</a>

											</td>

					
					
										<td class="list-hits">
						23					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/alert-box">
							Alert Box</a>

											</td>

					
					
										<td class="list-hits">
						19					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/algorithm">
							Algorithm</a>

											</td>

					
					
										<td class="list-hits">
						27					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/android">
							Android</a>

											</td>

					
					
										<td class="list-hits">
						27					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/antivirus">
							Antivirus</a>

											</td>

					
					
										<td class="list-hits">
						34					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/api">
							API</a>

											</td>

					
					
										<td class="list-hits">
						30					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/application">
							Application</a>

											</td>

					
					
										<td class="list-hits">
						46					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/archive">
							Archive</a>

											</td>

					
					
										<td class="list-hits">
						20					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/array">
							Array</a>

											</td>

					
					
										<td class="list-hits">
						3					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/batch-process">
							Batch Process</a>

											</td>

					
					
										<td class="list-hits">
						24					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/beta-software">
							Beta Software</a>

											</td>

					
					
										<td class="list-hits">
						40					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/bios">
							BIOS</a>

											</td>

					
					
										<td class="list-hits">
						31					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/browser">
							Browser</a>

											</td>

					
					
										<td class="list-hits">
						37					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/bug">
							Bug</a>

											</td>

					
					
										<td class="list-hits">
						25					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/c/c">
							C/C++</a>

											</td>

					
					
										<td class="list-hits">
						15					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/cad">
							CAD</a>

											</td>

					
					
										<td class="list-hits">
						9					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/cell">
							Cell</a>

											</td>

					
					
										<td class="list-hits">
						9					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/cgi">
							CGI</a>

											</td>

					
					
										<td class="list-hits">
						9					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/character-encoding">
							Character Encoding</a>

											</td>

					
					
										<td class="list-hits">
						16					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/checksum">
							Checksum</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/class">
							Class</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/clip-art">
							Clip Art</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/clipboard">
							Clipboard</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/codec">
							Codec</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/command-prompt">
							Command Prompt</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/commercial-software">
							Commercial Software</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/compile">
							Compile</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/compiler">
							Compiler</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/compression">
							Compression</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/contextual-menu">
							Contextual Menu</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/control-panel">
							Control Panel</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/copy">
							Copy</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/cron">
							Cron</a>

											</td>

					
					
										<td class="list-hits">
						6					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/cursor">
							Cursor</a>

											</td>

					
					
										<td class="list-hits">
						1					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/cut">
							Cut</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/daemon">
							Daemon</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/dashboard">
							Dashboard</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/data-management">
							Data management</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/data-type">
							Data type</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/software/dbms">
							DBMS</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/software/direct-x">
							Direct X</a>

											</td>

					
					
										<td class="list-hits">
						0					</td>
					
								</tr>
							</tbody>
	</table>

	
	<div>
		<!-- @TODO add hidden inputs -->
		<input type="hidden" name="filter_order" value="" />
		<input type="hidden" name="filter_order_Dir" value="" />
		<input type="hidden" name="limitstart" value="" />
	</div>
</form>
	</div>

	</div>

<div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
											<form action="/index.php" method="post" id="login-form" >
	<div class="pretext">
		</div>
	<fieldset class="userdata">
	<p id="form-login-username">
		<label for="modlgn-username">User Name</label>
		<input id="modlgn-username" type="text" name="username" class="inputbox"  size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn-passwd">Password</label>
		<input id="modlgn-passwd" type="password" name="password" class="inputbox" size="18"  />
	</p>
		<p id="form-login-remember">
		<label for="modlgn-remember">Remember Me</label>
		<input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
	</p>
		<input type="submit" name="Submit" class="button" value="Log in" />
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="user.login" />
	<input type="hidden" name="return" value="aW5kZXgucGhwP29wdGlvbj1jb21fY29udGVudCZJdGVtaWQ9MTQ3JmlkPTE1JnZpZXc9Y2F0ZWdvcnk=" />
	<input type="hidden" name="49e183d5eca06c5e5f8b7e69a59d9482" value="1" />	</fieldset>
	<ul>
		<li>
			<a href="/user/reset">
			Forgot your password?</a>
		</li>
		<li>
			<a href="/user/remind">
			Forgot your username?</a>
		</li>
				<li>
			<a href="/user/registration">
				Create an account</a>
		</li>
			</ul>
	<div class="posttext">
		</div>
</form>
					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="147" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:51:"software - টেকনোলজি বেসিক";s:11:"description";s:8:"software";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:8:"software";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:51:"software - টেকনোলজি বেসিক";s:6:"author";N;}}s:5:"links";a:3:{i:0;s:106:"<link href="/software?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0"";i:1;s:109:"<link href="/software?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0"";i:2;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:1:{s:15:"text/javascript";s:509:"
		window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});function keepAlive() {	var myAjax = new Request({method: "get", url: "index.php"}).send();} window.addEvent("domready", function(){ keepAlive.periodical(840000); });";}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}