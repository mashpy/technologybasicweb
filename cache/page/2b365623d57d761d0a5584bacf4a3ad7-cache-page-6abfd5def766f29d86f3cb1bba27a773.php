<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10992:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="কী, কমান্ড, যেমন, কীবোর্ডে, করা, এর, কাজ, থাকে, যায়।, জন্য, বা, ইত্যাদি।, এভাবে, ওপেন, যা" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Keyboard - hardware - টেকনোলজি বেসিক" />
  <meta name="author" content="মিনহাজুল হক শাওন" />
  <meta name="description" content="Keyboard - hardware কীবোর্ড হল কম্পিউটারের জন্য প্রয়োজনীয় ইনপুট ডিভাইস। এটা প্রাথমিক ইনপুট ডিভাইস এর মাঝে একটি। কীবোর্ড আসলে একটা বোর্ড যেখানে অনেকগুলো কী বা বাটন বসানো থাকে। কীবোর্ডের ধারণা আসে সাধারণ টাইপরাইটার থেকে। এর বর্ণনা সজ্জা এমন হয় যে দ্রুত লেখা" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Keyboard - hardware - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104" class="current active"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	টেকনোলজি বেসিক - হার্ডওয়্যার টার্ম</h1>
		<h2>
										Keyboard						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by মিনহাজুল হক শাওন				</dd>
	
		<dd class="hits">
		Hits: 1		</dd>
 </dl>

	
	<p>কীবোর্ড হল কম্পিউটারের জন্য প্রয়োজনীয় ইনপুট ডিভাইস। এটা প্রাথমিক  ইনপুট ডিভাইস এর মাঝে একটি। কীবোর্ড আসলে একটা বোর্ড যেখানে অনেকগুলো কী বা  বাটন বসানো থাকে।</p>
<p>কীবোর্ডের ধারণা আসে সাধারণ টাইপরাইটার থেকে। এর বর্ণনা সজ্জা এমন হয় যে  দ্রুত লেখার সময় যেন জ্যাম না বেধে যায়। তাই A B C D E ... এভাবে না করে A  S D F G এভাবে লেআউট ডিজাইন করা হয়। সবচেয়ে প্রচলিত লেআউট হল <strong>QWERTY </strong>যা কীবোর্ডের বামে উপরের কোনা থেকে প্রথম ছয়টি অক্ষর নিয়ে নাম করা।</p>
<p>অক্ষর ও নম্বর কী ছাড়াও কীবোর্ডে আরো কী থাকে যার দ্বারা বিভিন্ন কমান্ড  দেওয়া সম্ভব। যেমন কন্ট্রোল কী (Ctrl), ট্যাব কী (Tab), ডিলিট কী (Del)  ইত্যাদি। এগুলোর মাধ্যমে অনেক নির্দিষ্ট কমান্ড দেওয়া যায়। যেমন কন্ট্রোল +  সি (Ctrl + C) কমান্ড দিয়ে কোন ফাইল বা টেক্সট ক্লিপবোর্ডে কপি করা যায়।  আবার অপারেটিং সিস্টেম অনুযায়ী কিছু কী কাজ করে। যেমন উইন্ডোজ ওএস-এ <strong>উইন </strong>কী (Win) স্টার্ট মেনু ওপেন করে। এটা লিনাক্সে কাজ করেনা। আবার ম্যাকে <strong>কমান্ড </strong>কী (Cmd) নামে একটি কী থাকে যা তাদের নিজস্ব কমান্ড ব্যবহার করতে দেয়।</p>
<p>কীবোর্ডে আরো থাকে এক সারি ফাংশনাল কী (Fn), নিউমেরিক প্যাড ইত্যাদি।  তাছাড়া বিশেষ কিছু কাজ বা অ্যাপ্লিকেশান এর জন্য কী থাকতে পারে। যেমন  মাল্টিমিডিয়া কীবোর্ডে মিডিয়া প্লেয়ার প্লে/পজ, ভলিউম বাড়ানো/কমানো, ওয়েব  ব্রাউজার ওপেন করা ইত্যাদি কাজের জন্য অতিরিক্ত বাটন থাকতে পারে।</p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/hardware/jumper" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/hardware/kvm-switch" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/hardware/keyboard" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/hardware/keyboard" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="104" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:62:"Keyboard - hardware - টেকনোলজি বেসিক";s:11:"description";s:649:"Keyboard - hardware কীবোর্ড হল কম্পিউটারের জন্য প্রয়োজনীয় ইনপুট ডিভাইস। এটা প্রাথমিক ইনপুট ডিভাইস এর মাঝে একটি। কীবোর্ড আসলে একটা বোর্ড যেখানে অনেকগুলো কী বা বাটন বসানো থাকে। কীবোর্ডের ধারণা আসে সাধারণ টাইপরাইটার থেকে। এর বর্ণনা সজ্জা এমন হয় যে দ্রুত লেখা";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:211:"কী, কমান্ড, যেমন, কীবোর্ডে, করা, এর, কাজ, থাকে, যায়।, জন্য, বা, ইত্যাদি।, এভাবে, ওপেন, যা";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:62:"Keyboard - hardware - টেকনোলজি বেসিক";s:6:"author";s:44:"মিনহাজুল হক শাওন";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}