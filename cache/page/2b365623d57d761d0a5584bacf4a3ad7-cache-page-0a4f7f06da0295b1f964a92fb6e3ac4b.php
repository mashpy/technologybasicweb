<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12230:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="জন্য, করা, হয়ে, এজ্যাক্স, আরো, যদি, ক্লিক, এজ্যাক্সের, তাহলে, নতুন, সার্ভার, পেজ, ওয়েব, এর, আমরা" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Ajax - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Ajax - internet এজ্যাক্স পূর্ণ অর্থ Asynchronous JavaScript And XML." />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Ajax - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Ajax						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by মাসপি				</dd>
	
		<dd class="hits">
		Hits: 117		</dd>
 </dl>

	
	<p>এজ্যাক্স  পূর্ণ অর্থ "Asynchronous  <a href="internet/javascript"><span style="text-decoration: underline;">JavaScript</span></a> And XML." অর্থাৎ অ্যাসিংক্রোনাস  <span style="text-decoration: underline;"><a href="internet/javascript">জাভাস্ক্রিপ্ট</a> </span>আর এক্সএমএল এর  মিলিতরূপ হল এজ্যাক্স । <span style="text-decoration: underline;"><a href="internet/dynamic-website">ডায়নামিক ওয়েবসাইট</a> </span> তৈরির জন্য এটি বহুল পরিমানে  ব্যবহৃত হয়ে থাকে। আর ডায়নামিক কন্টেন্ট  দেখানোর জন্য ওয়েব ডিজাইনাররা  তাদের ওয়েবসাইটে জাভাস্ক্রিপ্ট আর এক্সএমএল  একসাথে ব্যবহার করে। ডেস্কটপ  অ্যাপ্লিকেশনের মত ওয়েব অ্যাপ্লিকেশন তৈরি  করার ইচ্ছা থেকেই  <span style="text-decoration: underline;"><a href="internet/ajax">এজ্যাক্স</a></span> এর  সৃষ্টি। ওয়েব ব্যবহারকারীদের একটি বোতামে  ক্লিক করা, নতুন পেজ  আসার আগ  পর্যন্ত অপেক্ষা করা, তারপর আরেকটি বোতাম  ক্লিক করা এগুলোর জন্য  অপেক্ষা  করতে হয়। এজ্যাক্সের ব্যবহারের মাধ্যমে এর  সমাধান সম্ভব। 'এজ্যাক্সের' অংশ 'অ্যাসিংক্রোনাস' হল ওয়েব ব্রাউজার থেকে  ওয়েবসার্ভারের কাছে পেজ বা  কনটেন্টের জন্য পাঠানো অনুরোধ, এই অনুরোধ  করা ও সার্ভার থেকে তথ্য  পাওয়ার মধ্যবর্তী সময়ে ব্যবহারকারী বর্তমান  পাতায় তার স্বাভাবিক কাজ  করতে পারেন। এজ্যাক্স এত শক্তিশালী কারন এটি  <span style="text-decoration: underline;"><a href="internet/client">ক্লায়েন্ট</a></span> সাইডে রান হতে পারে।  এরমানে জাভাস্ক্রীপ্ট একটি ওয়েবপেজ লোড হবার  পরেও <span style="text-decoration: underline;"><a href="internet/web-server">সার্ভার</a></span>কে রিকোয়েস্ট  পাঠাতে পারে। ফলে পেজে অন্য কনটেন্ট রিলোড হওয়া  ছাড়াই ডাটা সার্ভার হতে  রিসিভ হয়ে পেজে দেখাতে পারে। যদি সার্ভার সাইড  স্ক্রিপ্টিং ল্যাঙ্গুয়েজ <span style="text-decoration: underline;"><a href="internet/php">পিএইচপি</a></span> বা <a href="internet/asp"><span style="text-decoration: underline;">এএসপি</span></a> ইউজ করা হত তাহলে সম্পূর্ণ পেজটিকে  নতুন কন্টেন্ট দেখানোর জন্য  রিলোড প্রয়োজন হত।</p>
<p><img src="images/internet/AE/ajax.gif" border="0" /></p>
<p>উপরের ব্যাখ্যাগুলো যদি বুঝতে না পারা যায় তাহলে আরো সহজভাবে ব্যাখ্যা   করা যাক। ফেসবুকের কথাই ধরা যাক, যখন আমরা ফেসবুকের news feed এ অন্যের   স্টাটাসগুলো দেখি। আরো স্টাস্টাস দেখার জন্য পেজের নিচের অংশে older post এ   ক্লিক করি। ফলে পেজ রিলোড হওয়া ছাড়া আমরা আরো নতুন স্টাস্টাস দেখতে পাই।   এটা এজ্যাক্সের কারণে হয়ে থাকে। আবার আমরা যখন গুগলে কোন কিছু সার্চ দিতে   যাই তখন যদি barak লিখি তাহলে দেখা যায় যে সার্চ ইঞ্জিন <a href="internet/google"><span style="text-decoration: underline;">গুগল</span></a> সয়ংক্রিয়ভাবে  obama  লেখাটিকেও সাজেস্ট করে এটা এজ্যাক্সের কারণেই হয়ে থাকে ।</p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/apache" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/affiliate" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://technologybasic.com/internet/ajax" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://technologybasic.com/internet/ajax" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:58:"Ajax - internet - টেকনোলজি বেসিক";s:11:"description";s:102:"Ajax - internet এজ্যাক্স পূর্ণ অর্থ Asynchronous JavaScript And XML.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:235:"জন্য, করা, হয়ে, এজ্যাক্স, আরো, যদি, ক্লিক, এজ্যাক্সের, তাহলে, নতুন, সার্ভার, পেজ, ওয়েব, এর, আমরা";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:58:"Ajax - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}