<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11394:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="এএসপি, থাকে।, সার্ভিস, দিয়ে, বিভিন্ন, ওয়েব, এবং, নিয়ে, জন্য, ধরনের, কোন, এরা, ছোট, তথ্য, স্ক্রীপ্ট" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="ASP - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="ASP - internet এএসপি-র দুই ধরনের অর্থ আছে। একটি Active Server Page এবং অন্যটি Application Service Provider . Active Server Page এটা একটা ওয়েবপেজ যেখানে এএসপি স্ক্রীপ্ট সংযুক্ত করা হয়ে থাকে। এটা কম্পিউটারের ছোট প্রোগ্রামের মতই যা রান হয় তখন, যখন এএসপি বেসড" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>ASP - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										ASP						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by মাসপি				</dd>
	
		<dd class="hits">
		Hits: 54		</dd>
 </dl>

	
	<p>এএসপি-র দুই ধরনের অর্থ আছে। একটি "Active Server Page" এবং অন্যটি "Application Service   Provider".</p>
<p>*** Active Server Page : এটা একটা ওয়েবপেজ যেখানে এএসপি  স্ক্রীপ্ট   সংযুক্ত করা হয়ে থাকে। এটা কম্পিউটারের ছোট প্রোগ্রামের মতই যা  রান হয় তখন, যখন এএসপি বেসড ওয়েব পেজ সনাক্ত হয়। অ্যাক্টিভ সার্ভার পেজের  এক্সটেনশনগুলো .এএসপি(.ASP)   আকারে থাকে। ভিজিটরের ওয়েব ব্রাউজারে আসার আগে এএসপি  পেজগুলো সার্ভারে  প্রসেস  হয়ে থাকে। যেসব ওয়েবসাইটের ওয়েব পেজগুলো  পরিবর্তনশীল যেগুলোতে এএসপি  ইউজ  করা হয়। উদাহরনসরূপ- এএসপি স্ক্রীপ্ট ভিজিটরের  জিপ কোড নিয়ে বা  ভিজিটর থেকে  কোন তথ্য নিয়ে ঐ ওয়েবপেজের কন্টেন্টগুলো  কাস্টমাইজ করে তারপর  ভিজিটরকে  দেখায়। এএসপি টেকনোলজী ডিজাইন করেছে  মাইক্রোসফট। আর এএসপি  স্ক্রীপ্ট লেখা  হয়েছে মাইক্রোসফট ভিজুয়াল বেসিক দিয়ে।</p>
<p><img src="images/activeserver.jpg" border="0" /></p>
<p>***  Application Service Providerঃ এর মানে হল তৃতীয় কোন কোম্পানি যারা    কাস্টমারকে সফটওয়্যার বেসড সার্ভিস দিয়ে থাকে। বিভিন্ন প্রতিষ্ঠান   বিভিন্ন  তথ্য স্বল্পমূল্যে এবং সহজে পাওয়ার জন্য এদের সার্ভিস নিয়ে থাকে।   এদের পাঁচ  ধরনের সার্ভিস রয়েছে।</p>
<p>ক. লোকাল বা রিজিওনাল এএসপি: ছোট ব্যবসা  প্রতিষ্ঠানকে বিভিন্ন ধরনের  এপ্লিকেশন সার্ভিস এরা দিয়ে থাকে।</p>
<p>খ.স্পেশালিস্ট এএসপিঃ বিশেষ কোন নির্দিষ্ট দরকারের জন্য এরা নির্দিষ্ট  সার্ভিস  দিয়ে থাকে। যেমন- ওয়েব সার্ভিস।</p>
<p>গ.ভার্টিকাল মার্কেট এএসপি: এরা বিশেষ কিছু  প্রতিষ্ঠানকে সার্ভিস দিয়ে থাকে  যেমন - শিক্ষা প্রতিষ্ঠান।</p>
<p>ঘ. এন্টারপ্রাইজ এএসপি: বড় বড় ব্যবস্যা প্রতিষ্ঠানের জন্য বিভিন্ন সার্ভিস  এবং  তথ্য সুবিধা দিয়ে থাকে।</p>
<p>ঙ. ভলিউম বিজনেস এএসপি: ছোট এবং মাঝারি আয়তনের  শিল্পকে বিশাল আয়তনে  সার্ভিস দিয়ে থাকে।</p>
<p><img src="images/internet/AE/applicationserver.gif" border="0" /></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/aspnet" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/archie" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/asp" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/asp" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:57:"ASP - internet - টেকনোলজি বেসিক";s:11:"description";s:535:"ASP - internet এএসপি-র দুই ধরনের অর্থ আছে। একটি Active Server Page এবং অন্যটি Application Service Provider . Active Server Page এটা একটা ওয়েবপেজ যেখানে এএসপি স্ক্রীপ্ট সংযুক্ত করা হয়ে থাকে। এটা কম্পিউটারের ছোট প্রোগ্রামের মতই যা রান হয় তখন, যখন এএসপি বেসড";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:238:"এএসপি, থাকে।, সার্ভিস, দিয়ে, বিভিন্ন, ওয়েব, এবং, নিয়ে, জন্য, ধরনের, কোন, এরা, ছোট, তথ্য, স্ক্রীপ্ট";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:57:"ASP - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}