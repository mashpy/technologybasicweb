<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10337:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="সার্চ, গুগল, গুগলের, এবং, ফলে, ইঞ্জিন, এই, ওয়েব, সার্চ, হয়।, ইঞ্জিনের, সালে, আরো, একটি, internet" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Google - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Google - internet গুগল হল বিশ্বের মধ্যে জনপ্রিয় একটি সার্চ ইঞ্জিন। ১৯৯৬ সালে স্ট্যানফোর্ড ইউনিভার্সিটির ছাত্র সার্গেই ব্রেন এবং ল্যারি পেজ এই সার্চ প্রজেক্টটি শুরু করেন। তারা এমন একটি সার্চ ইঞ্জিন এলগরিদম ডেভেলাপ করেছিলেন যেখানে সার্চ রেজাল্ট শুধুমাত্র কন" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Google - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Google						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by মাহমুদ রহমান				</dd>
	
		<dd class="hits">
		Hits: 47		</dd>
 </dl>

	
	<p>গুগল হল বিশ্বের  মধ্যে জনপ্রিয় একটি  সার্চ ইঞ্জিন। ১৯৯৬ সালে স্ট্যানফোর্ড  ইউনিভার্সিটির ছাত্র সার্গেই  ব্রেন  এবং ল্যারি পেজ এই সার্চ প্রজেক্টটি শুরু করেন।  তারা এমন একটি সার্চ  ইঞ্জিন এলগরিদম ডেভেলাপ করেছিলেন যেখানে সার্চ  রেজাল্ট শুধুমাত্র কনটেন্ট ও  কীওয়ার্ডের উপর নির্ভর করবে না বরং কতটি  ওয়েবসাইট ঐ ওয়েবপেজটির ব্যাকলিংক  রেখেছে তাও পর্যবেক্ষন  করে। এই পদ্ধতির ফলে দেখা যায় যে, গুগলের  সার্চের ফলাফলগুলো আরো  বিষয়ভিত্তিক এবং অন্যান্য সার্চ ইঞ্জিন থেকে ভালো  হচ্ছে। ফলে ওয়েব সার্চ  ইঞ্জিনের মার্কেট শেয়ারে গুগল অত্যন্ত ভাল এবং  শক্তিশালী অবস্থানে আসে।  গুগলের সার্চ ইঞ্জিনের রেঙ্কিংকে পরবর্তীতে  পেজরেঙ্ক নাম দেয়া হয় এবং ২০০১  সালে এটা পেন্টেন্ট হয়। খুব কম সময়েই গুগল  এক নাম্বার সার্চ ইঞ্জিনে পরিণত  হয়।</p>
<p><strong> </strong> বর্তমানে গুগলের অনেক প্রোডাক্ট রয়েছে যার বেশির  ভাগই বিনামূল্যে  ব্যবহার করা যায়। ফলে গুগলরের জনপ্রিয়তা আরো বৃদ্ধি  পাচ্ছে। গুগলের  উল্লেখযোগ্য প্রোডাক্টগুলো হচ্ছে - ওয়েব সার্চ, ইমেজ  সার্চ, গুগল ম্যাপ,  গুগল টুলবার, বুকমার্ক, গুগল রিডার, গুগল এডসেন্স,  গুগল ডকস, ব্লগার  ইত্যাদি।</p>
<p><img src="images/internet/FJ/google.jpeg" border="0" /></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/gopher" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/gateway" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/google" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/google" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:60:"Google - internet - টেকনোলজি বেসিক";s:11:"description";s:659:"Google - internet গুগল হল বিশ্বের মধ্যে জনপ্রিয় একটি সার্চ ইঞ্জিন। ১৯৯৬ সালে স্ট্যানফোর্ড ইউনিভার্সিটির ছাত্র সার্গেই ব্রেন এবং ল্যারি পেজ এই সার্চ প্রজেক্টটি শুরু করেন। তারা এমন একটি সার্চ ইঞ্জিন এলগরিদম ডেভেলাপ করেছিলেন যেখানে সার্চ রেজাল্ট শুধুমাত্র কন";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:216:"সার্চ, গুগল, গুগলের, এবং, ফলে, ইঞ্জিন, এই, ওয়েব, সার্চ, হয়।, ইঞ্জিনের, সালে, আরো, একটি, internet";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:60:"Google - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}