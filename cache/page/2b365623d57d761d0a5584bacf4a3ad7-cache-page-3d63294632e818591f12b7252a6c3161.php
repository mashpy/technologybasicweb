<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12468:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="।, করা, যায়, স্ট্যাটিক, ওয়েবসাইটের, খুব, ওয়েবসাইট, ওয়েব, বা, সহজে, ধরনের, থাকে, পেজ, তৈরী, সাইট" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Static Website - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Static Website - internet Static শব্দের অর্থ স্থির বা অনড় । স্ট্যাটিক ওয়েব সাইটের পেজগুলোতে নির্দিষ্ট কনটেন্ট থাকে । প্রত্যেকটি পেজ এইচটিএমএল (হাইপারটেক্সট মার্কআপ ল্যাঙ্গুয়েজ) দ্বারা কোডিং করা থাকে । এ ধরনের ওয়েব সাইট গুলো খুব সহজে, স্বল্প সময়ে তৈরী করা " />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Static Website - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Static Website						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by সৈকত de Velliers				</dd>
	
		<dd class="hits">
		Hits: 33		</dd>
 </dl>

	
	<p>Static শব্দের অর্থ “স্থির বা অনড়”  । স্ট্যাটিক ওয়েব সাইটের পেজগুলোতে  নির্দিষ্ট কনটেন্ট  থাকে । প্রত্যেকটি  পেজ এইচটিএমএল (হাইপারটেক্সট  মার্কআপ ল্যাঙ্গুয়েজ) দ্বারা কোডিং করা থাকে ।  এ ধরনের ওয়েব সাইট গুলো খুব  সহজে, স্বল্প সময়ে তৈরী করা যায় । এ ...ধরনের  ওয়েব সাইট হল গতিশীল বা  ডায়নামিক ওয়েবসাইটের সম্পূর্ণ বিপরীত । কারন এ  ধরনের ওয়েবসাইট তৈরী করতে  কোন ধরনের ওয়েব প্রোগ্রামিং বা ডাটাবেস ডিজাইনিং  এর খুব বেশি প্রয়োজন হয়  না । সাধারনত ছোট ওয়েবসাইট যেগুলোতে মাত্র কয়েকটি  পেজ থাকবে সেগুলো  স্ট্যাটিক আকারে তৈরি করা হয় । তবে যেসব ওয়েবসাইটের পেজের  সংখ্যা অনেক  থাকবে সেসব ডায়নামিক আকারে তৈরি করা হয়। বানিজ্যকভাবে বা  প্রসাশনিকভাবে  কখনই স্ট্যাটিক ওয়েবসাইটে পরিবর্তে ডায়নামিক ওয়েবসাইট বেশি  তৈরি করা হয় ।  স্ট্যাটক ওয়েবসাইটের টেমপ্লেট প্রকাশের ক্ষেত্রে  সি,এস,এস(সিসকেডিং স্টাইল  শিট)ব্যবহার করা হয়। মোট কথা স্ট্যাটিক ওয়েবপেজে  যেসব কনটেন্ট থাকে  এগুলোর লিঙ্ক একই হয়ে থাকে। <a href="http://www.facebook.com/www.techterms.com">www.techterms.com</a> এটাকেও  স্ট্যাটিক সাইট বলা যায়। আপনি এই লিঙ্কে ঢুকলে সারাদিনই এই পেজ  দেখাবে  যতক্ষন পর্যন্ত ওয়েবডেভেলাপার নির্দিষ্ট কোন আপডেট না দেয়।</p>
<p>আবার  ডায়নামিকে আসি । যেমন- <a href="http://www.facebook.com/www.facebook.com">www.facebook.com</a> । আপনি আপনার নাম এবং পাসওয়ার্ড দিয়ে  লগিন করলে একই লিঙ্কে যা দেখাবে তা নিশ্চয় আপনার বন্ধুর একাউন্টে ভিন্ন  দেখাবে।</p>
<p>স্ট্যাটিক ওয়েবসাইটের সুবিধা সমূহ:</p>
<p>১.এ ধরনের ওয়েবসাইট খুব  সহজে তৈরী ও নিয়ন্ত্রন করা যায় ।</p>
<p>২.ডায়ানমিক ওয়েবসাইটের চেয়ে খুব সহজে  ডিজাইন করা যায় ।</p>
<p>৩.খুব সহজেই সার্চ ইঞ্জিন পরিচালনা করা যায় ।</p>
<p>৪.কনটেন্টগুলো  সহজে নিয়ন্ত্রন করা যায় ।</p>
<p>৫.সহজেই ওয়েবপেজের লে-আউট চেঞ্জ করা যায় ।</p>
<p>৬.খুব  দ্রুততার সাথে এসব ওয়েবসাইট থেকে খুব কম নেট স্পীডএও ডাটা নামানো যায় ।</p>
<p>স্ট্যাটিক  ওয়েবসাইটের অসুবিধা সমূহ:</p>
<p>১.কনটেন্ট আপডেট করতে প্রচুর সময় লাগে ।</p>
<p>২.ওয়েবসাইটের  আকার বৃদ্ধি পেলে কনটেন্টগুলো সহজে নিয়ন্ত্রন করা যায় না ।</p>
<p>৩.ওয়েবডিজাইনিং  অত্যন্ত কষ্টসাধ্য এবং সময়সাপেক্ষ ।</p>
<p><img src="images/internet/AE/dynamic-website.jpg" border="0" /></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/streaming" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/ssl" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://technologybasic.com/internet/static-website" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://technologybasic.com/internet/static-website" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:68:"Static Website - internet - টেকনোলজি বেসিক";s:11:"description";s:621:"Static Website - internet Static শব্দের অর্থ স্থির বা অনড় । স্ট্যাটিক ওয়েব সাইটের পেজগুলোতে নির্দিষ্ট কনটেন্ট থাকে । প্রত্যেকটি পেজ এইচটিএমএল (হাইপারটেক্সট মার্কআপ ল্যাঙ্গুয়েজ) দ্বারা কোডিং করা থাকে । এ ধরনের ওয়েব সাইট গুলো খুব সহজে, স্বল্প সময়ে তৈরী করা ";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:229:"।, করা, যায়, স্ট্যাটিক, ওয়েবসাইটের, খুব, ওয়েবসাইট, ওয়েব, বা, সহজে, ধরনের, থাকে, পেজ, তৈরী, সাইট";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:68:"Static Website - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}