<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10414:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="suggestion, আপনার, আপনি, মূল্যবান, মতামত, পারেন।, দিতে, নিচে, করার, ধন্যবাদ, আপনাকে, আমাদের, ভিজিট, সাইটের, জন্য।" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="suggestion - suggestion - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="suggestion - suggestion ধন্যবাদ আপনাকে আমাদের সাইটের ভিজিট করার জন্য। নিচে আপনি আপনার মূল্যবান মতামত দিত" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>suggestion - suggestion - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	আপনার পরামর্শ</h1>
		<h2>
										যা যা আপডেট করা হয়েছে						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by Super User				</dd>
	
		<dd class="hits">
		Hits: 112		</dd>
 </dl>

	
	<p>আজ<strong> ২৩ ই আগস্ট </strong>. এই পর্যন্ত যা যা আপডেট হয়েছে ।</p>
<p><strong>১। সফটওয়্যার টার্মে </strong><a href="../software/checksum">Checksum      থেকে </a><a href="../software/direct-x">Direct X      পর্যন্ত আপডেট করা হয়েছে ।<br /></a></p>
<p>আরো যা যা আপডেট হবে -</p>
<p>১। হার্ডওয়্যার টার্মে ছবি যোগ করা হবে । আর যে সব টার্মের ব্যাখ্যা পর্যাপ্ত নয় সেগুলো আপডেট করা হবে।</p>
<p>২। সফটওয়্যার টার্মের ছবি যোগ সহ সবগুলো শেষ করা হবে।</p>
<p>৩। বাকী সব গুলো টার্মের কাজ শেষ করা হবে।</p>
<p>৪। সব টার্ম শেষ না হওয়া পর্যন্ত ব্যাকলিঙ্ক বসানো যাচ্ছে না।</p>
<p><strong>৫। আমরা আমাদের ভিজিটরকে নির্ভুল তথ্য দিতে চাই । আপনারা যদি কোন ভুল দেখতে পান তাহলে ইমেইল করে আমাদের জানিয়ে দেয়ার জন্য অনুরোধ রইল । </strong></p>
<p>--------------------------------------------------------------------</p>
<p><strong>ভিজিটরঃ </strong> সাইটে ইতোমধ্যে প্রচুর পরিমান ভিজিটর। আর্টিকেল দেখার সংখ্যা ১৪০০০ (দশ হাজার) ছাড়িয়েছে । আমাদের ডোমেইন এবুং হোস্টিং এ সহযোগিতা করছে - <strong>" লিনাক্স পাঠশালা "</strong></p>
<h1><span class="profileName fn ginormousProfileName fwb">আপনি যোগাযোগ করতে পারেন- </span></h1>
<h1><span class="profileName fn ginormousProfileName fwb"> ফেসবুক আইডি - <a href="http://www.facebook.com/technologybasic"> টেকনোলজি বেসিক</a></span></h1>
<p><span class="profileName fn ginormousProfileName fwb">ইমেইল - <strong>
 <script type='text/javascript'>
 <!--
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy67956 = '&#97;dm&#105;n' + '&#64;';
 addy67956 = addy67956 + 't&#101;chn&#111;l&#111;gyb&#97;s&#105;c' + '&#46;' + 'c&#111;m';
 document.write('<a ' + path + '\'' + prefix + ':' + addy67956 + '\'>');
 document.write(addy67956);
 document.write('<\/a>');
 //-->\n </script><script type='text/javascript'>
 <!--
 document.write('<span style=\'display: none;\'>');
 //-->
 </script>JLIB_HTML_CLOAKING
 <script type='text/javascript'>
 <!--
 document.write('</');
 document.write('span>');
 //-->
 </script></strong><br /></span></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/suggestion/problem-with-bengali-font" rel="next">&lt; Prev</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/suggestion/suggestion" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/suggestion/suggestion" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="148" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:66:"suggestion - suggestion - টেকনোলজি বেসিক";s:11:"description";s:240:"suggestion - suggestion ধন্যবাদ আপনাকে আমাদের সাইটের ভিজিট করার জন্য। নিচে আপনি আপনার মূল্যবান মতামত দিত";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:263:"suggestion, আপনার, আপনি, মূল্যবান, মতামত, পারেন।, দিতে, নিচে, করার, ধন্যবাদ, আপনাকে, আমাদের, ভিজিট, সাইটের, জন্য।";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:66:"suggestion - suggestion - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}