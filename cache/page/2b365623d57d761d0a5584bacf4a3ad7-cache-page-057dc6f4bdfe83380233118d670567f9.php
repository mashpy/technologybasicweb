<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:13251:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="ডোমেইন, করে, নেম, কোন, বা, হয়, রেজিস্ট্রেশন, বলা, এর, গোল্ড, করতে, ।, হয়, অনেক, পারে।" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Cybersquatter - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Cybersquatter - internet আমেরিকার শুরুর দিকে এর পশ্চিমাঞ্চল ছিল বেশ রূক্ষ। সেসময় জমিসংক্রান্ত কোন নির্দিষ্ট নিয়ম বা মালিকানা না থাকায় ভাগ্যান্বেষীরা বিভিন্ন দিক থেকে এসে সরকারি জমি দখল করে নিজ নামে দাবি করত। এদেরকে বলা হত স্কোয়াটার । ১৮০০ শতকের দিকে ক্যাল" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Cybersquatter - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Cybersquatter 						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by নাবিল আহমেদ				</dd>
	
		<dd class="hits">
		Hits: 33		</dd>
 </dl>

	
	<p>আমেরিকার শুরুর দিকে এর পশ্চিমাঞ্চল  ছিল বেশ রূক্ষ। সেসময় জমিসংক্রান্ত কোন  নির্দিষ্ট নিয়ম বা মালিকানা না  থাকায় ভাগ্যান্বেষীরা বিভিন্ন দিক থেকে এসে  সরকারি জমি দখল করে নিজ নামে  দাবি করত। এদেরকে বলা হত “স্কোয়াটার”। ১৮০০  শতকের দিকে ক্যালিফোর্ণিয়া  গোল্ড রাশের সময় এধরণের স্কোয়াটারদের  উল্লেখযোগ্য উপস্থিতি দেখা যায়।</p>
<p>১৯৯০-এ নতুন আরেক গোল্ড রাশের শুরু হয়,  তবে এবারের রাশ ছিল গোল্ড এর  বদলে ডোমেইন নেমের। নব ইন্টারনেট যুগের অনেক  ব্যবহারকারি ডোমেইন নেমের  মূল্য বুঝতে পেরে যে যত পারল ডোমেইন নেম  রেজিস্ট্রেশন করতে লাগল। মাত্র  কয়েক বছরেই সব কমন “ডট কমস” গুলো  রেজিস্টার্ড হয়ে গেলো। অনেকেই শুধুমাত্র  ওয়েবসাইট খোলার চেয়ে বিনিয়োগের  জন্যে রেজিস্টার করল। এদেরকে বলা হয়  “সাইবারস্কোয়াটার” এবং এ পদ্ধতিকে বলা  হয় “সাইবারস্কোয়াটিং”।</p>
<p>সাইবারস্কোয়াটারদের নিজস্ব ডোমেইন এর সংখ্যা  কয়েকটি হতে পারে.... কয়েক  হাজারো হতে পারে। তারা রেজিস্ট্রেশন করে সেসব  ডোমেইন নেমগুলো যেগুলোতে  জনপ্রিয় শব্দ কিংবা প্রবাদ রয়েছে। আবার অনেক  ডোমেইনার আছে যাদেরকে বলা হয়  “টাইপোস্কোয়াটার” ডোমেইন নেম রেজিস্ট্রেশন করে  যেগুলো জনপ্রিয় কিছু  ওয়েবসাইটের অনুরূপমাত্র কিন্তু মুদ্রাক্ষর ভিন্ন।  এধরণের ডোমেইনের মূল  উদ্দেশ্য হল ভুলে লিখা URLগুলোর দ্বারা ট্রাফিক সৃষ্টি  করা। সাধারণত  সাইবারস্কোয়াটাররা লাভ করে দুইটি উপায়ে.....(১) পেজগুলোতে   অ্যাডভারটাইজ়মেন্টে ক্লিক করার মাধ্যমে। (২) যারা কিনতে আগ্রহী তাদের কাছে   ডোমেইনগুলো উল্লেখযোগ্য মূল্যে বিক্রি করে দেয়া। অনেকে প্রকৃত মালিকদের   জোর করত ডোমেইন মালিকানা ছেড়ে দেয়ার জন্য। এর ফলে ১৯৯৯ সালে   “Anticybersquatting Consumer Protection Act ”(ACPA) প্রবর্তিত হয়, যেটা   ডোমেইন সত্বাধীকারিদের বৈধ মালিকানার জন্য একটি স্বতন্ত্র্য ট্রেডমার্ক বা   রেজিস্টার্ড নাম দিয়ে থাকে। সাধারণ কথায়, কোন ইউজার এমন কোন ডোমেইন নেম   রেজিস্ট্রেশন করতে পারবে না যার কোন অনুরূপ বা সমার্থক আছে।</p>
<p>যদি ডোমেইন  নেম নিয়ে কোন ধরণের সন্দেহ সৃষ্টি হয় সেক্ষেত্রে সেটি আইন  পর্যন্ত গড়াবে  যা সমাধানে পরবর্তীতে দীর্ঘসময় নিতে পারে। এটি প্রতিরোধে  ICANN প্রবর্তন  করেছে “Uniform Domain Name Dispute Resolution Policy” বা  UDRP যেটা  ডোমেইন</p>
<p>সত্বাধীকারিদের নিজস্ব ট্রেডমার্ক দেয়ার মাধ্যমে  সাইবারস্কোয়াটারদের  প্রতিরোধ করেছে। যদিও এখনো সাইবারস্কোয়াটিং আছে, তবে  মালিকরা উন্নত  ব্যবস্থার কারণে এখন খুব সহজে নিজের ডোমেইন নেম রক্ষা করতে  পারছে।</p>
<p><img src="images/internet/AE/cybersquatting.jpg" border="0" /></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/delicious" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/cyberspace" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/cybersquatter" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/cybersquatter" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:67:"Cybersquatter - internet - টেকনোলজি বেসিক";s:11:"description";s:639:"Cybersquatter - internet আমেরিকার শুরুর দিকে এর পশ্চিমাঞ্চল ছিল বেশ রূক্ষ। সেসময় জমিসংক্রান্ত কোন নির্দিষ্ট নিয়ম বা মালিকানা না থাকায় ভাগ্যান্বেষীরা বিভিন্ন দিক থেকে এসে সরকারি জমি দখল করে নিজ নামে দাবি করত। এদেরকে বলা হত স্কোয়াটার । ১৮০০ শতকের দিকে ক্যাল";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:199:"ডোমেইন, করে, নেম, কোন, বা, হয়, রেজিস্ট্রেশন, বলা, এর, গোল্ড, করতে, ।, হয়, অনেক, পারে।";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:67:"Cybersquatter - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}