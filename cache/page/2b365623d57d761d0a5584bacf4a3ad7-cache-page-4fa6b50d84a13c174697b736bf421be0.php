<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9665:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="গফার, জন্য, ।, এবং, প্রোগ্রাম, এই, এর, বিভিন্ন, তথ্য, ব্যবহার, ইন্টারফেস, ব্যবহ্নত, এটা, যেখানে, করে।" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="gopher - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="gopher - internet গফার (gopher) প্রযুক্তিটি মিন্নেসেটা বিশ্ববিদ্যালয়ে আবিস্কৃত হয়েছিল । এই গফার প্রক্রিয়াট" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>gopher - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										gopher 						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by ফাহমিদা পলি				</dd>
	
		<dd class="hits">
		Hits: 55		</dd>
 </dl>

	
	<p><span class="text_exposed_show"><strong> </strong>গফার (gopher)  প্রযুক্তিটি মিন্নেসেটা  বিশ্ববিদ্যালয়ে আবিস্কৃত হয়েছিল । এই গফার  প্রক্রিয়াটি জনগনকে কোনো তথ্য  খোঁজা ও ফিরিয়ে আনার অনুমতি দেয় টেক্সট  ইন্টারফেস ব্যবহার করে। এই  প্রযুক্তিটি ক্লায়েন্ট-সার্ভার কাঠামোর উপর  প্রতিষ্ঠিত যেখানে যেখানে একজন  গফার ক্লায়েন্ট প্রোগ্রাম গফার সার্ভারকে  খোঁজার জন্য ব্যবহ্নত হয়। সার্ভার  বিভিন্ন তথ্য , প্রোগ্রাম , ডকুমেন্ট,  আর্টিকল ইত্যাদি জমা করে রাখে।  হাইপারলিঙ্ক এর পরিবর্তে , গফার ইন্টারফেস  বিভিন্ন ডকুমেন্ট এবং প্রোগ্রাম  দেখানোর জন্য লিঙ্কের মেনু ব্যবহার করে।  মিন্নেসেটা বিশ্ববিদ্যালয় , ১৯৯৩  সালে গফার প্রযুক্তির জন্য একটি  লাইসেন্সিং প্রোগ্রাম আরম্ভ করেছিলো ,  যেহেতু গফার প্রযুক্তির ব্যবহার  ইন্টারনেট এর মাধ্যমে দ্রুত ছড়িয়ে পড়েছিলো ।  যাই হোক, এটা ওয়ার্ল্ড ওয়াইড  ওয়েব এর কাছাকাছি সময়ে সূচিত হয়েছিলো । কারন  এতে ব্যবহার হত হাইপারটেক্সট  এবং ইমেজ এবং এটা খুব শীঘ্রই একটা পছন্দনীয়  মাধ্যম হয়ে উঠেছিলো বিভিন্ন  তথ্য খোঁজা এবং ব্রাউজ করার জন্য ।</span></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/handle" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/google" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/gopher" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/gopher" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:60:"gopher - internet - টেকনোলজি বেসিক";s:11:"description";s:246:"gopher - internet গফার (gopher) প্রযুক্তিটি মিন্নেসেটা বিশ্ববিদ্যালয়ে আবিস্কৃত হয়েছিল । এই গফার প্রক্রিয়াট";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:247:"গফার, জন্য, ।, এবং, প্রোগ্রাম, এই, এর, বিভিন্ন, তথ্য, ব্যবহার, ইন্টারফেস, ব্যবহ্নত, এটা, যেখানে, করে।";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:60:"gopher - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}