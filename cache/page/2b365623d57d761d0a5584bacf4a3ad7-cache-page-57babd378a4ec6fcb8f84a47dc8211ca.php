<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11243:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="এর, এটিএম, কার্ড, এবং, এই, ইন্টারনেট, ।, একটি, অথবা, যার, একটা, করে।, এটি, অর্থ, করতে" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="ATM - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="ATM - internet এটিএম-র দুটি অর্থ আছে." />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>ATM - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										ATM						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by ফাহমিদা পলি				</dd>
	
		<dd class="hits">
		Hits: 65		</dd>
 </dl>

	
	<p>এটিএম-র দুটি অর্থ আছে...</p>
<p>***এটিএম  এর পূর্ণরূপ হল "Automated Teller Machine" , এছাড়া এটি ক্যাশ মেশিন, ক্যাশ পয়েন্ট, অটোমেটিক ব্যাংকিং মেশিন, হোল ইন দ্য ওয়াল(ব্রিটিশ ইংলিশ) ইত্যাদি   নামেও পরিচিত।</p>
<p>এটা একটা কম্পিউটারাইজড টেলিযোগাযোগ যন্ত্র যেটা ক্লায়েন্ট এবং অর্থদাতা   প্রতিষ্ঠানের মধ্যে কোনো ক্যাশিয়ার ছাড়াই অর্থ বিনিময় করে। বর্তমানে এটিএম-এ, কাষ্টমারকে সনাক্ত করা হয় প্লাষ্টিক মোড়ানো এটিএম কার্ড দিয়ে, যার   সাথে থাকে একটা ম্যাগনেটিক স্ট্রাইপ অথবা প্লাষ্টিক স্মার্ট কার্ড এর সাথে  একটি  চিপ। এই এটিএম কার্ড এর একটি নাম্বার থাকে। এছাড়া এর সিকিউরিটি হিসেবে মেয়াদোত্তীর্ণ তারিখ অথবা সিভিভিসি(cvvc) এবং অথেন্টিকেশন হিসেবে পিন কোড থাকে। এটিএম কার্ড ব্যবহার করে কাষ্টমার  তার একাউন্ট থেকে টাকা  উঠাতে পারে, নগদ টাকা দিতে পারে এবং তাদের ব্যালেন্স  চেক করতে পারে । এটি  টাকা লেনদেনের একটি সহজ পদ্ধতি।</p>
<p><img src="images/internet/AE/atm.jpg" border="0" /></p>
<p>*** এটিএম: এর আরেকটি নাম আছে "Asynchronous Transfer Mode" যার অর্থ দাড়ায়   একসাথে পরিবর্তন হয় না এমন । কম্পিউটার দুনিয়াতে এটিএম হল নেটওয়ার্কিং   প্রযুক্তি যেটা ডাটাকে প্যাকেট অথবা একটা নির্দিষ্ট সাইজে পরিনত করে। এটিএম   ব্যবহার করে ৫৩ বাইট (৫ বাইট এড্রেস হেডার ও ৪৮ বাইট তথ্যর জন্য ) সেলস(cells) । এই সেলগুলি ছোটো এবং এদের গতি ৬০০ এমবিপিএস যা এই ডাটা পরিবর্তন করার জন্য   যথেষ্ট । এই প্রযুক্তির ডিজাইন  করা হয়েছিল উচ্চগতি সম্পন্ন মিডিয়া জগতের   সাধারন গ্রাফিক্সকে ফুল মোশন ভিডিওতে পরিনত করার জন্য । যেহেতু   সেলগুলো খুব ছোট, তাই এগুলো একক কানেকশনের মধ্যে অনেক বেশি পরিমান ডাটা   সরবরাহ করতে পারে। এটি ইন্টারনেট সার্ভিস প্রভাইডারদের প্রতিটি কাস্টমারের   নির্দিষ্ট পরিমান ব্যান্ডউইথ নির্দেশ করতে সহায়তা করে। ফলে ইন্টারনেট   সার্ভিস প্রোভাইডারদের ইন্টারনেট সংযোগের দক্ষতা বাড়ায়, যার ফলে সর্বত্র ইন্টারনেট সংযোগ এর গতি বৃদ্ধি পায়।</p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/autoresponder" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/aspnet" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/atm" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/atm" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:57:"ATM - internet - টেকনোলজি বেসিক";s:11:"description";s:71:"ATM - internet এটিএম-র দুটি অর্থ আছে.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:199:"এর, এটিএম, কার্ড, এবং, এই, ইন্টারনেট, ।, একটি, অথবা, যার, একটা, করে।, এটি, অর্থ, করতে";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:57:"ATM - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}