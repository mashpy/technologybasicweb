<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10759:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="আরপিএম, এর, করে, সিপিএম, হয়, ওয়েবসাইট, হতে, আয়, (rpm), এবং, প্রতিদিন, চালিত, বিভাগ, এডস, পরিমান" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="RPM - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="RPM - internet পদার্থবিদ্যার ভাষায় আরপিএম (RPM) এর পূর্ন রূপ হচ্ছে রেভ্যুলেশনস পার মিনিট (Revolutions per minute) কিন্তু অনলাইন জগতে আরপিএম (RPM) এর পূর্ন রূপ হচ্ছে রেভেন্যু পার ১০০০ ইম্প্রেশান্স (Revenue per 1000 impressions)। সিপিএম (CPM) এর সাথে আরপিএম" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>RPM - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										RPM						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by ফাহমিদা পলি				</dd>
	
		<dd class="hits">
		Hits: 37		</dd>
 </dl>

	
	<p>পদার্থবিদ্যার ভাষায়  আরপিএম (RPM) এর পূর্ন রূপ হচ্ছে "রেভ্যুলেশনস পার  মিনিট"(Revolutions per  minute) কিন্তু অনলাইন জগতে আরপিএম (RPM) এর পূর্ন  রূপ হচ্ছে "রেভেন্যু পার  ১০০০ ইম্প্রেশান্স " (Revenue per 1000  impressions)।  সিপিএম (CPM) এর  সাথে আরপিএম (RPM) এর কিছুটা মিল রয়েছে।  কিন্তু এটা পরিমাপ করে রাজস্ব  বিভাগ হতে ১০০০ এডস এর আয়ের পরিমান  কস্ট অফ এডস এর পরিবর্তে। যেখানে  সিপিএম চালিত হয় বিজ্ঞাপনদাতার দ্বারা,  সেখানে আরপিএম চালিত হয় পাবলিশার  দ্বারা। উদ্বাহরনস্বরূপ একজন  পাবলিশারের নিজস্ব ওয়েবসাইট থাকে যেটা  প্রতিদিন প্রায় ৫০০০ পেজ দ্বারা  পরিক্ষিত হয়। যদি বিজ্ঞাপনটির ওয়েবসাইট তার  প্রচার হতে প্রতিদিন মোট  ৳২৫.00 আয় করে, তাহলে এই ওয়েবসাইট এর আরপিএম হল  ৳৫. 00(২৫?৫)ওয়েব  পাবলিশাররা আরপিএম ব্যবহার করে পরিমাপের উপায় হিসেবে  কিভাবে বিজ্ঞাপন  কার্যকর হয়ে প্রচার বিভাগ  হতে আয় করে। যদি এই খুব কম  পরিমান আয় করে ,  তাহলে পাবলিশাররা অন্য বিজ্ঞাপন পছন্দ করে যেটার আরপিএম  হার বেশি এবং উচ্চ  রাজস্ব বিভাগের হবে। আরপিএম শুধু অনলাইনের বিজ্ঞাপনেই  ব্যবহ্নত হয় না বরং  এটা অন্যান্য বিজ্ঞাপনের পরিমাপের জন্যও ব্যবহার করা  হয়। যদিও আরপিএম  কিছুটা কনফিউজিং তারপরেও আরপিএম এবং সিপিএম প্রায়ই সমতুল্য  হয় । সিপিএম  সাধারনভাবে সমার্থক হিসেবে আরপিএম এর সাথে ব্যবহ্নত হয়ে  পাবলিশারদের  দৃষ্টিপাত থেকে এভারেজ রেভুন্যু বর্ননা করে।<strong> </strong></p>
<p><strong><img src="images/internet/PT/rpm.jpg" border="0" /><br /></strong></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/rss" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/radcab" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/rpm" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/rpm" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:57:"RPM - internet - টেকনোলজি বেসিক";s:11:"description";s:537:"RPM - internet পদার্থবিদ্যার ভাষায় আরপিএম (RPM) এর পূর্ন রূপ হচ্ছে রেভ্যুলেশনস পার মিনিট (Revolutions per minute) কিন্তু অনলাইন জগতে আরপিএম (RPM) এর পূর্ন রূপ হচ্ছে রেভেন্যু পার ১০০০ ইম্প্রেশান্স (Revenue per 1000 impressions)। সিপিএম (CPM) এর সাথে আরপিএম";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:219:"আরপিএম, এর, করে, সিপিএম, হয়, ওয়েবসাইট, হতে, আয়, (rpm), এবং, প্রতিদিন, চালিত, বিভাগ, এডস, পরিমান";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:57:"RPM - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}