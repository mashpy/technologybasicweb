<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9882:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="এর, পারে, কোনো, করতে, ব্যবহারকারীর, কুকি, হতে, বিভিন্ন, cookie, যে, করে, ।, তথ্য, মূল, একজন" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Cookie - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Cookie - internet কুকি বিভিন্ন নামে পরিচিত যেমন ঃ HTTP cookie, web cookie, browser cookie ইত্যাদি। এটা তার ওয়েব ব্রাউজারের এ" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Cookie - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <link rel="stylesheet" href="/plugins/content/mavikthumbnails/mavikthumbnails/slimbox-mt1.2/css/slimbox.css" type="text/css" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>
  <script type="text/javascript" src="/plugins/content/mavikthumbnails/mavikthumbnails/slimbox-mt1.2/js/slimbox.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Cookie						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by ফাহমিদা পলি				</dd>
	
		<dd class="hits">
		Hits: 57		</dd>
 </dl>

	
	<p><span class="text_exposed_show">কুকি বিভিন্ন নামে পরিচিত যেমন : HTTP  cookie, web cookie, browser cookie ইত্যাদি। এটা তার ওয়েব ব্রাউজারের এর  মাধ্যমে কোনো ব্যবহারকারী তার কম্পিউটার থেকে যে সব ডাটা পাঠায় তা সংরক্ষণ  করে রাখে । এটা ব্যবহৃত হয় একজন ব্যবহারকারীর ওয়েব ব্রাউজারের এর ঠিকানায়   মূল ওয়েবসাইট থেকে কোনো তথ্য পাঠাতে। আবার ঐ  ব্যবহারকারীর তথ্য  মূল সাইট  এর ঠিকানায় পাঠাতে। এই তথ্য ব্যবহ্নত হতে পারে একজন ব্যবহারকারীর  সময়কাল,  অভিরুচি , শপিং কার্ট নিয়ে ধারনা অথবা অন্য যে কোনো প্রমান ও  সনাক্তকরনের  জন্য যেটা সম্পন্ন হতে পারে জমাকৃত ডাটার মাধ্য্যমে । কুকি কোনো সফটওয়্যার  নয় , তারা প্রোগ্রাম করতে পারে না, ভাইরাস বহন করতে পারে না , হার্ড ড্রাইভ  এর ভিতর লুকিয়ে থাকা প্রোগ্রামগুলোকে বের করে আনতে পারে না। যাই হোক,  গোয়েন্দা সংস্থাগুলোর কর্মকান্ডে  গতিবিধি পর্যবেক্ষণ করতে এটি ব্যবহার হয়।  EUROPE এবং US এর বিভিন্ন আইন প্রয়োগকারী সংস্থায় কুকি ব্যবহার হয়।</span></p>
<p><span class="text_exposed_show"><a style="" class="thumbnail" href="images/internet/AE/cookies.png" rel="lightbox[50]" title="" target="_blank"><img src="images/stories/thumbnails/images-internet-AE-cookies-491x395.png" border="0" width="491" height="395" /></a><br /></span></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/cpa" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/cloud-computing" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/cookie" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/cookie" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:60:"Cookie - internet - টেকনোলজি বেসিক";s:11:"description";s:234:"Cookie - internet কুকি বিভিন্ন নামে পরিচিত যেমন ঃ HTTP cookie, web cookie, browser cookie ইত্যাদি। এটা তার ওয়েব ব্রাউজারের এ";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:205:"এর, পারে, কোনো, করতে, ব্যবহারকারীর, কুকি, হতে, বিভিন্ন, cookie, যে, করে, ।, তথ্য, মূল, একজন";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:60:"Cookie - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:1:{s:78:"/plugins/content/mavikthumbnails/mavikthumbnails/slimbox-mt1.2/css/slimbox.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";s:76:"/plugins/content/mavikthumbnails/mavikthumbnails/slimbox-mt1.2/js/slimbox.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}