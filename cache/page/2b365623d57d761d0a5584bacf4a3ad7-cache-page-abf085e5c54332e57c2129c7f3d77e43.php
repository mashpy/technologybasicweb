<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12328:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="ব্লগ, একটা, করেন, আর, অন্য, মন্তব্য, কোন, খবর, ব্লগই, বেশিরভাগ, ১৯৯৯-এ, বা, •, জোর, জন্ম" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Blog - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Blog - internet যিনি ব্লগে পোস্ট করেন তাকে ব্লগার বলার হয়। ব্লগাররা প্রতিনিয়ত তাদের ওয়েবসাইটে কনটেন্ট যুক্ত করেন আর ব্যবহারকারীরা সেখানে তাদের মন্তব্য করতে পারেন। এছাড়াও সাম্প্রতিক কালে ব্লগ ফ্রিলান্স সাংবাদিকতার একটা মাধ্যম হয়ে উঠছে। সাম্প্রতিক ঘটনা" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Blog - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Blog						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by তুসিন আহমেদ				</dd>
	
		<dd class="hits">
		Hits: 59		</dd>
 </dl>

	
	<p><span class="text_exposed_show"> </span></p>
<p>যিনি  ব্লগে পোস্ট করেন তাকে ব্লগার  বলার হয়। ব্লগাররা প্রতিনিয়ত তাদের  ওয়েবসাইটে কনটেন্ট যুক্ত করেন আর  ব্যবহারকারীরা সেখানে তাদের মন্তব্য  করতে পারেন। এছাড়াও সাম্প্রতিককালে  ব্লগ ফ্রিলান্স সাংবাদিকতার একটা  মাধ্যম হয়ে উঠছে। সাম্প্রতিক ঘটনাসমূহ  নিয়ে এক বা একাধিক ব্লগাররা  এটি নিয়মিত আপডেট করেন।</p>
<p>বেশিরভাগ  ব্লগই কোন একটা নির্দিষ্ট বিষয়সম্পর্কিত ধারাবিবরণী বা খবর  জানায়;  অন্যগুলো আরেকটু বেশিমাত্রায় ব্যক্তিগত পর্যায়ের অনলাইন  দিনপত্রী/অনলাইন  দিনলিপিসমূহ। একটা নিয়মমাফিক ব্লগ লেখা, ছবি, অন্য ব্লগ,  ওয়েব পেজ আর  এবিষয়ের অন্য মাধ্যমের লিংকের সমাহার/সমষ্টি। পাঠকদের  মিথষ্ক্রিয়াময়  ছাঁচে মন্তব্য করার সুবিধে-রাখা বেশিরভাগ ব্লগের একটা  গুরুত্বপূর্ণ দিক।  প্রায় ব্লগই মূলত লেখায় আকীর্ণ, কিছু কিছু আবার জোর  দেয় শিল্প (আর্ট  ব্লগ), ছবি (ফটোব্লগ), ভিডিও (ভিডিও ব্লগিং), সঙ্গীত  (এমপিথ্রিব্লগ) আর  অডিওর (পডকাস্টিং) ওপর। মাইক্রোব্লগিং-ও আরেকধরনের  ব্লগিং, ওটায় খুব ছোট  ছোট পোস্ট থাকে। ডিসেম্বর, ২০০৭-এর হিসেবে, ব্লগ  খোঁজারু ইঞ্জিন  টেকনোরাট্টি প্রায় এগারো কোটি বার লাখেরও বেশি ব্লগের  হদিশ পেয়েছে।</p>
<p>১৯৯৮ সালে ব্রুস আবেলসন ওপেন  ডায়রি নামান, এতে করে হাজারো অনলাইন  দিনপত্রী জন্ম নেয়। ওপেন ডায়রির  আবিষ্কার হচ্ছে পাঠক মন্তব্য, এটাই ছিলো  প্রথম ব্লগ কমিউনিটি যেখানে  পাঠকেরা অন্য লেখকের ব্লগ অন্তর্ভুক্তিতে  মন্তব্য করতে পারতেন।</p>
<p>•  ১৯৯৯-এর মার্চে ব্র্যাড ফিটজপ্যাট্রিক শুরু করেন লাইভ জার্নাল।</p>
<p>•  জুলাই, ১৯৯৯-এ এন্ড্রু স্মেলস কোন ওয়েবসাইটে একটা "খবর পাতা" রাখার   বিকল্প হিসেবে জন্ম দেন পিটাস.কম-এর, এর পরপরই সেপ্টেম্বর, ১৯৯৯-এ আসে   ডায়েরিল্যান্ড, যেখানে ব্যক্তিগত দিনপত্রীমূলক কমিউনিটির ওপর জোর দেওয়া   হয়। [১১] ইভান উইলিয়ামস এবং মেগ হুরিহান (পাইরা ল্যাবস) ব্লগার.কম চালু   করেন অগস্ট, ১৯৯৯-এ। (গুগল এটা কিনে নেয় ২০০৩-এর ফেব্রুয়ারিতে)</p>
<p><span class="text_exposed_hide"><span class="text_exposed_link"><a><img src="images/internet/AE/blog.jpg" border="0" /><br /></a></span></span></p>
<p> </p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/bookmark" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/bittorrent" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/blog" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/blog" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:58:"Blog - internet - টেকনোলজি বেসিক";s:11:"description";s:667:"Blog - internet যিনি ব্লগে পোস্ট করেন তাকে ব্লগার বলার হয়। ব্লগাররা প্রতিনিয়ত তাদের ওয়েবসাইটে কনটেন্ট যুক্ত করেন আর ব্যবহারকারীরা সেখানে তাদের মন্তব্য করতে পারেন। এছাড়াও সাম্প্রতিক কালে ব্লগ ফ্রিলান্স সাংবাদিকতার একটা মাধ্যম হয়ে উঠছে। সাম্প্রতিক ঘটনা";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:206:"ব্লগ, একটা, করেন, আর, অন্য, মন্তব্য, কোন, খবর, ব্লগই, বেশিরভাগ, ১৯৯৯-এ, বা, •, জোর, জন্ম";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:58:"Blog - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}