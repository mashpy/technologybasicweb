<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12327:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="করে, আইপি, ইমেইল, করা, কোন, সনাক্ত, করতে, স্পামাররা, নকল, স্পুফিং(ip, স্পুফিং(spoofing), হয়, অ্যাড্রেস, ইউজারকে, জন্য" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Spoofing - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Spoofing - internet স্পুফ(Spoof)অর্থ প্রতারণা করা,ধোঁকা দেওয়া।আইটি জগতেও স্পুফিং(Spoofing) মানে কোন কম্পিউটার সিস্টেম বা ইউজারকে ধোঁকা দেওয়া।সাধারনত কোন ইউজাররের আইডি গোপন করে অথবা নকল করে স্পুফিং(Spoofing) করা হয়।বিভিন্ন পদ্বতিতে স্পুফিং(Spoofing) করা যা" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Spoofing - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Spoofing						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by পাভেল ফাহমি				</dd>
	
		<dd class="hits">
		Hits: 28		</dd>
 </dl>

	
	<p>স্পুফ(Spoof) অর্থ  প্রতারণা করা, ধোঁকা দেওয়া। আইটি জগতেও  স্পুফিং(Spoofing)  মানে কোন  কম্পিউটার সিস্টেম বা ইউজারকে ধোঁকা দেওয়া। সাধারনত কোন ইউজাররের  আইডি গোপন  করে অথবা নকল করে স্পুফিং(Spoofing) করা হয়। বিভিন্ন পদ্ধতিতে   স্পুফিং(Spoofing) করা যায়। তবে  সবচেয়ে বেশি স্পুফিং(Spoofing) করা হয়   ইমেইলের মাধ্যমে। ইমেইল স্পুফিং(Spoofing) পদ্ধতিতে  ভুয়া ইমেইল অ্যাড্রেস   থেকে ইউজারের কাছে ইমেইল পাঠিয়ে ইউজারকে বোঁকা বানানো হয়। তবে প্রায় সব মেইল   সার্ভারেই এর বিরুদ্ধে প্রতিরোধ ব্যবস্থা থাকে। কিন্তু  স্পামাররা  আরও   ধুরন্ধর । তারা   স্পাম মেসেজ পাঠানোর জন্য তাদের নিজস্ব  SMTP (“Simple   Mail Transfer Protocol”) ব্যবহার করে। যার মাধ্যমে স্পামাররা অন্য ইউজারের   ইমেইল অ্যাড্রেস নকল করে আপনাকে ইমেইল পাঠাতে সক্ষম হয়। যেমন ধরুন আপনার   ইমেইল আইডি 
 <script type='text/javascript'>
 <!--
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy87967 = 'xnm&#101;' + '&#64;';
 addy87967 = addy87967 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.write('<a ' + path + '\'' + prefix + ':' + addy87967 + '\'>');
 document.write(addy87967);
 document.write('<\/a>');
 //-->\n </script><script type='text/javascript'>
 <!--
 document.write('<span style=\'display: none;\'>');
 //-->
 </script>JLIB_HTML_CLOAKING
 <script type='text/javascript'>
 <!--
 document.write('</');
 document.write('span>');
 //-->
 </script>।এখন স্পামাররা SMTP সাহায্যে আপনার আইডিকে হুবহু   নকল করে অন্য কোন ইউজারকে ইমেইল পাঠাতে পারবে।আরেকটি স্পুফিং(Spoofing)   পদ্বতি হচ্ছে আইপি স্পুফিং(ip Spoofing)।</p>
<p>এ পদ্ধতিতে কোন কম্পিউটার   সিস্টেমের আইপি আড্রেস গোপন অথবা নকল করা হয়। ইন্টারনেটে যখন কেউ আপনাকে   তথ্য পাঠাবে আপনি আইপি অ্যাড্রেসের মাধ্যমে ঐ তথ্যের উৎস সনাক্ত করতে   পারবেন। কিন্তু যখন আইপি  স্পুফিং(ip Spoofing) করা হয় অর্থাৎ ভুয়া আইপি   অ্যাড্রেস ব্যবহার করা হয়,তখন আপনি সহজে ঐ তথ্যের সঠিক  উৎস সনাক্ত করতে   পারবেন না। স্পামাররা যাতে অপরাধ করে ধরা না পড়ে সে জন্য  আইপি স্পুফিং(ip   Spoofing)করে। যেমন স্পামাররা  এক সাথে অনেক ডাটা প্রেরন করে কোন সার্ভার ক্র্যাশ করে ফেলল। এখন কেউ যাতে তাদের অবস্থান সনাক্ত করতে না পারে সে জন্য   তারা আইপি স্পুফিং(ip Spoofing) করে।এধরনের আক্রমনকে বলা হয় denial of   service attacks।  কিছু কিছু সিকিউরিটি সফটওয়্যার রয়েছে যারা এধরনের আক্রমন   সনাক্ত করে তা প্রতিহত করতে পারে।</p>
<p><img src="images/internet/PT/spoofing.jpg" border="0" /></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/ssh" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/spider" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/spoofing" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/spoofing" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:62:"Spoofing - internet - টেকনোলজি বেসিক";s:11:"description";s:593:"Spoofing - internet স্পুফ(Spoof)অর্থ প্রতারণা করা,ধোঁকা দেওয়া।আইটি জগতেও স্পুফিং(Spoofing) মানে কোন কম্পিউটার সিস্টেম বা ইউজারকে ধোঁকা দেওয়া।সাধারনত কোন ইউজাররের আইডি গোপন করে অথবা নকল করে স্পুফিং(Spoofing) করা হয়।বিভিন্ন পদ্বতিতে স্পুফিং(Spoofing) করা যা";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:269:"করে, আইপি, ইমেইল, করা, কোন, সনাক্ত, করতে, স্পামাররা, নকল, স্পুফিং(ip, স্পুফিং(spoofing), হয়, অ্যাড্রেস, ইউজারকে, জন্য";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:62:"Spoofing - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}