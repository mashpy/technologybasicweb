<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:13444:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="কোন, রিলিজ, বাগ, সমস্যা, করা, হয়।, করতে, পারে।, প্রোগ্রামটি, যে, করার, থাকলে, অনেক, তবে, যদি" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Bug - software - টেকনোলজি বেসিক" />
  <meta name="author" content="নাবিল আহমাদ" />
  <meta name="description" content="Bug - software কম্পিউটার জগতে বাগ হল কোন সফটওয়্যার প্রোগ্রামে সৃষ্ট এরর। এর ফলে প্রোগ্রামটি অপ্রত্যাশিতভাবে থেমে যেতে পারে অথবা অপ্রয়োজনীয় আচরন করতে পারে। যেমনঃ কোন প্রোগ্রামে বাগের ফলে প্রোগ্রামটির কোন কোন বাটনে ক্লিক করলে নাও কাজ করতে পারে। তবে মারাত্মক" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Bug - software - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>
  <script type="text/javascript">
function keepAlive() {	var myAjax = new Request({method: "get", url: "index.php"}).send();} window.addEvent("domready", function(){ keepAlive.periodical(840000); });
  </script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147" class="current active"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	টেকনোলজি বেসিক - সফটওয়্যার টার্ম</h1>
		<h2>
										Bug						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by নাবিল আহমাদ				</dd>
	
		<dd class="hits">
		Hits: 24		</dd>
 </dl>

	
	<p><span class="text_exposed_show">কম্পিউটার  জগতে বাগ হল কোন সফটওয়্যার  প্রোগ্রামে সৃষ্ট এরর। এর ফলে প্রোগ্রামটি  অপ্রত্যাশিতভাবে থেমে যেতে পারে  অথবা অপ্রয়োজনীয় আচরন করতে পারে। যেমনঃ কোন  প্রোগ্রামে বাগের ফলে  প্রোগ্রামটির কোন কোন বাটনে ক্লিক করলে নাও কাজ করতে  পারে। তবে মারাত্মক  বাগ থাকলে অনেক সময় পুরো সফটওয়্যারটি ক্র্যাশ কিংবা  হ্যাং করতে পারে।<br /> <br /> একজন ডেভেলপারের দৃষ্টিকোণ থেকে, বাগ হল একটি  প্রোগ্রামের সোর্স কোডে  বিন্যাস কিংবা লজিকের সমস্যা। এধরণের সমস্যা  ‘ডিবাগার’ নামক ডেভেলপমেন্ট  টুল দ্বারা নির্ধারন করা যায়। তবে প্রোগ্রামটি  কম্পাইল করার পরও যদি  চূড়ান্তভাবে কোন সমস্যা ধরা না পড়ে... শুধুমাত্র  তাহলেই কোন বাগ ইউজারের  কাছে দৃশ্যমান হয়।<br /> যেহেতু বাগের কারণে কোন  প্রোগ্রাম ব্যবহারে নেতিবাচক প্রভাব পড়ে, সেজন্যে  বেশীরভাগ প্রোগ্রাম  সাধারণের জন্য রিলিজ করার পূর্বে অনেকবার পরীক্ষা করা  হয়।  উদাহরণস্বরূপ  বলা যায়, বেটা সফটওয়্যারের কথা। আমরা সবাই জানি যে,  বাণিজ্যিক  সফটওয়্যারগুলো সাধারণে রিলিজ হওয়ার আগে প্রথমে বেটা স্তরের মধ্য  দিয়ে যায়,  যেখানে অসংখ্য ইউজার এর বিভিন্ন অংশ ব্যবহারের মাধ্যমে কোন  সমস্যা থাকলে  সেটা বের করার চেষ্টা করে। যদি নিশ্চিত হওয়া যায় যে,  প্রোগ্রামটি বাগ এবং  সমস্যা বা এরর মুক্ত... এরপর সেটি সাধারণে রিলিজ করা  হয়।<br /> অবশ্যই, আমরা  জানি যে, যদিও অনেক পরীক্ষার পর একটি সফটওয়্যার রিলিজ করা  হয়, তথাপি  বেশীরভাগ প্রোগ্রামই পুরোপুরি সমস্যামুক্ত নয়। একারণেই  ডেভেলপাররা “পয়েন্ট  আপডেট” (যেমনঃ ভার্সন 1.0.1) রিলিজ করে, যেগুলো আসলে  প্রথম রিলিজের পর বাগ  পাওয়া গেলে সেটা মুক্ত করে রিলিজ করা। তাই বিশেষভাবে  “বাগী” প্রোগ্রামগুলো  বাগমুক্ত হওয়ার জন্য অসংখ্য পয়েন্ট আপডেট (1.0.2,  1.0.3, ইত্যাদি) রিলিজ  করে।</span></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/software/browser" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/software/c/c" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/software/bug" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/software/bug" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
											<form action="/index.php" method="post" id="login-form" >
	<div class="pretext">
		</div>
	<fieldset class="userdata">
	<p id="form-login-username">
		<label for="modlgn-username">User Name</label>
		<input id="modlgn-username" type="text" name="username" class="inputbox"  size="18" />
	</p>
	<p id="form-login-password">
		<label for="modlgn-passwd">Password</label>
		<input id="modlgn-passwd" type="password" name="password" class="inputbox" size="18"  />
	</p>
		<p id="form-login-remember">
		<label for="modlgn-remember">Remember Me</label>
		<input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
	</p>
		<input type="submit" name="Submit" class="button" value="Log in" />
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="user.login" />
	<input type="hidden" name="return" value="aW5kZXgucGhwP29wdGlvbj1jb21fY29udGVudCZJdGVtaWQ9MTQ3JmNhdGlkPTE1JmlkPTMwNiZ2aWV3PWFydGljbGU=" />
	<input type="hidden" name="afa780f7c33167338cbb2a4cc9e09c68" value="1" />	</fieldset>
	<ul>
		<li>
			<a href="/user/reset">
			Forgot your password?</a>
		</li>
		<li>
			<a href="/user/remind">
			Forgot your username?</a>
		</li>
				<li>
			<a href="/user/registration">
				Create an account</a>
		</li>
			</ul>
	<div class="posttext">
		</div>
</form>
					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="147" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:57:"Bug - software - টেকনোলজি বেসিক";s:11:"description";s:661:"Bug - software কম্পিউটার জগতে বাগ হল কোন সফটওয়্যার প্রোগ্রামে সৃষ্ট এরর। এর ফলে প্রোগ্রামটি অপ্রত্যাশিতভাবে থেমে যেতে পারে অথবা অপ্রয়োজনীয় আচরন করতে পারে। যেমনঃ কোন প্রোগ্রামে বাগের ফলে প্রোগ্রামটির কোন কোন বাটনে ক্লিক করলে নাও কাজ করতে পারে। তবে মারাত্মক";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:220:"কোন, রিলিজ, বাগ, সমস্যা, করা, হয়।, করতে, পারে।, প্রোগ্রামটি, যে, করার, থাকলে, অনেক, তবে, যদি";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:57:"Bug - software - টেকনোলজি বেসিক";s:6:"author";s:31:"নাবিল আহমাদ";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:1:{s:15:"text/javascript";s:165:"function keepAlive() {	var myAjax = new Request({method: "get", url: "index.php"}).send();} window.addEvent("domready", function(){ keepAlive.periodical(840000); });";}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}