<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10821:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="আইপি, hhhh, ipv6, প্রতিটি, এড্রেস, বিট, ব্যবহার, করে, এই, ১২৮, সরবরাহ, ipv4, এর, সাথে, এবং" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="IPv6 - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="IPv6 - internet ইন্টারনেট সংযুক্ত প্রতিটি কম্পিউটার বা ডিভাইসের অবস্থান সনাক্ত করা হয় আইপি এড্রেস দ্বারা। বর্তমানকালের আইপি এড্রেস সরবরাহ করা হয় IPv4 সিস্টেমে। এই সিস্টেম ৩২-বিট ফরমেট মেনে চলে। যেমন - ১২৩. IPv6 সিস্টেমের উদ্ভব হয়েছে। IPv6 কে IPng ও বলা হয়" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>IPv6 - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										IPv6						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by মাসপি				</dd>
	
		<dd class="hits">
		Hits: 32		</dd>
 </dl>

	
	<p>ইন্টারনেট সংযুক্ত প্রতিটি কম্পিউটার বা ডিভাইসের অবস্থান সনাক্ত করা   হয় আইপি এড্রেস দ্বারা। বর্তমানকালের আইপি এড্রেস সরবরাহ করা হয় IPv4   সিস্টেমে। এই সিস্টেম ৩২-বিট ফরমেট মেনে চলে। যেমন - ১২৩.১২৪.১৫৬.২৩৫ ।   কম্পিউটার ও বিভিন্ন ডিভাইস বৃদ্ধির সাথে সাথে এবং প্রতিটি ডিভাইসকে   স্বতত্র আইপি দেয়ার জন্য আইপি এড্রেস বৃদ্ধি করা প্রয়োজনীয় হয়ে পড়ে। এই   কারণেই IPv6 সিস্টেমের উদ্ভব হয়েছে। IPv6 কে IPng ও বলা হয়। এর পূর্ণ অর্থ   IP next generation. তবে এর আগে IPv5 পরীক্ষামূলকভাবে ব্যবহার হয়েছিল।  IPv4  ৩২ বিট এড্রেস ব্যবহার করে আর IPv6 ১২৮ বিট এড্রেস ব্যবহার করে। ১২৮  বিট  ব্যবহারের ফলে আরো বেশি পরিমান অনন্য আইপি প্রতিটি ডিভাইসকে দেয়া যায়।   উদাহরনস্বরূপঃ IPv4 যদি ৩২ বিট ব্যবহার করে ৪২৯৪৯৬৭২৯৬(২^৩২) টি অনন্য  আইপি  সরবরাহ করে তাহলে IPv6 ভার্সন ১২৮ বিট ব্যবহার করে ২^১২৮ মানে   ৩৪০,০০০,০০০,০০০,০০০,০০০,০০০,০০০,০০০,০০০,০০০,০০০,০০</p>
<p>০ টি অনন্য আইপি এড্রেস সরবরাহ করতে পারবে। আর  এই সিস্টেমের আইপি  এড্রেসগুলো ধরন সাধারনত এরকম হয়ে থাকে -   hhhh:hhhh:hhhh:hhhh:hhhh:hhhh:hhhh:hhhh</p>
<p>এখানে প্রতিটি hhhh অংশ ৪  টি হেক্সাডেসিমাল সংখ্যা দ্বারা গঠিত। যার  মানে প্রতিটি ডিজিট ০-৯ এবং A-F  পর্যন্ত হতে পারে। তাই IPv6 এর আইপি  এড্রেসগুলোর ধরণ -</p>
<p>F704:0000:0000:0000:3458:79A2:D08B:4320</p>
<p>এই  কারনে আইপি এড্রেসগুলো অনেক জটিল প্রকৃতির, এবং প্রতিটি কম্পিউটার অধিক  নিরাপত্তা নিয়ে ইন্টারনেটের সাথে সংযুক্ত হতে পারে।</p>
<p><img src="images/internet/FJ/ipv.jpg" border="0" /></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/irc" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/ipv4" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://technologybasic.com/internet/ipv6" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://technologybasic.com/internet/ipv6" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:58:"IPv6 - internet - টেকনোলজি বেসিক";s:11:"description";s:617:"IPv6 - internet ইন্টারনেট সংযুক্ত প্রতিটি কম্পিউটার বা ডিভাইসের অবস্থান সনাক্ত করা হয় আইপি এড্রেস দ্বারা। বর্তমানকালের আইপি এড্রেস সরবরাহ করা হয় IPv4 সিস্টেমে। এই সিস্টেম ৩২-বিট ফরমেট মেনে চলে। যেমন - ১২৩. IPv6 সিস্টেমের উদ্ভব হয়েছে। IPv6 কে IPng ও বলা হয়";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:190:"আইপি, hhhh, ipv6, প্রতিটি, এড্রেস, বিট, ব্যবহার, করে, এই, ১২৮, সরবরাহ, ipv4, এর, সাথে, এবং";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:58:"IPv6 - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}