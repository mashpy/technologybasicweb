<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:31308:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="hardware" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="hardware - টেকনোলজি বেসিক" />
  <meta name="author" content="" />
  <meta name="description" content="hardware" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>hardware - টেকনোলজি বেসিক</title>
  <link href="/hardware?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />
  <link href="/hardware?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>
  <script type="text/javascript">

		window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});
  </script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104" class="current active"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিট এবং বাইট টার্ম</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="category-list">

		<h1>
		টেকনোলজি বেসিক - হার্ডওয়্যার টার্ম	</h1>
	
	
	
	<div class="cat-items">
		

<form action="http://www.technologybasic.com/hardware" method="post" name="adminForm" id="adminForm">
	
				<div class="display-limit">
			Display #&#160;
			<select id="limit" name="limit" class="inputbox" size="1" onchange="this.form.submit()">
	<option value="5">5</option>
	<option value="10">10</option>
	<option value="15">15</option>
	<option value="20">20</option>
	<option value="25">25</option>
	<option value="30">30</option>
	<option value="50">50</option>
	<option value="100">100</option>
	<option value="0" selected="selected">All</option>
</select>
		</div>
		
	
	<table class="category">
				<thead>
			<tr>
				<th class="list-title" id="tableOrdering">
					<a href="javascript:tableOrdering('a.title','asc','');" title="Click to sort by this column">Title</a>				</th>

				
				
								<th class="list-hits" id="tableOrdering4">
					<a href="javascript:tableOrdering('a.hits','asc','');" title="Click to sort by this column">Hits</a>				</th>
							</tr>
		</thead>
		
		<tbody>

									<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/access-point">
							Access Point</a>

											</td>

					
					
										<td class="list-hits">
						74					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/active-matrix">
							Active-Matrix  </a>

											</td>

					
					
										<td class="list-hits">
						43					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/adc">
							ADC</a>

											</td>

					
					
										<td class="list-hits">
						36					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/adf">
							ADF </a>

											</td>

					
					
										<td class="list-hits">
						34					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/adsl">
							ADSL</a>

											</td>

					
					
										<td class="list-hits">
						41					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/agp">
							AGP </a>

											</td>

					
					
										<td class="list-hits">
						51					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/alu">
							ALU</a>

											</td>

					
					
										<td class="list-hits">
						48					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/ata">
							ATA</a>

											</td>

					
					
										<td class="list-hits">
						41					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/backside-bus">
							Backside Bus</a>

											</td>

					
					
										<td class="list-hits">
						30					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/base-station">
							Base Station</a>

											</td>

					
					
										<td class="list-hits">
						26					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/blu-ray">
							Blu-ray</a>

											</td>

					
					
										<td class="list-hits">
						39					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/boot-disk">
							Boot Disk</a>

											</td>

					
					
										<td class="list-hits">
						47					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/bridge">
							Bridge</a>

											</td>

					
					
										<td class="list-hits">
						41					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/bus">
							Bus</a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/cable-modem">
							Cable Modem</a>

											</td>

					
					
										<td class="list-hits">
						44					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/card-reader">
							Card Reader</a>

											</td>

					
					
										<td class="list-hits">
						73					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/ccd">
							CCD</a>

											</td>

					
					
										<td class="list-hits">
						37					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/cd">
							CD</a>

											</td>

					
					
										<td class="list-hits">
						29					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/cd-r">
							CD-R</a>

											</td>

					
					
										<td class="list-hits">
						35					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/cd-rom">
							CD-ROM</a>

											</td>

					
					
										<td class="list-hits">
						32					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/chip">
							Chip</a>

											</td>

					
					
										<td class="list-hits">
						37					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/chipset">
							Chipset</a>

											</td>

					
					
										<td class="list-hits">
						36					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/cisc">
							CISC</a>

											</td>

					
					
										<td class="list-hits">
						31					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/clone">
							Clone</a>

											</td>

					
					
										<td class="list-hits">
						37					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/cluster">
							Cluster </a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/cmos">
							CMOS </a>

											</td>

					
					
										<td class="list-hits">
						29					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/compact-flash">
							Compact Flash</a>

											</td>

					
					
										<td class="list-hits">
						36					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/component">
							Component </a>

											</td>

					
					
										<td class="list-hits">
						36					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/computer">
							Computer</a>

											</td>

					
					
										<td class="list-hits">
						37					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/controller-card">
							Controller Card</a>

											</td>

					
					
										<td class="list-hits">
						38					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/cpu">
							CPU </a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/crt">
							CRT </a>

											</td>

					
					
										<td class="list-hits">
						39					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/dac">
							DAC</a>

											</td>

					
					
										<td class="list-hits">
						37					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/ddr">
							DDR</a>

											</td>

					
					
										<td class="list-hits">
						39					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/ddr2">
							DDR2</a>

											</td>

					
					
										<td class="list-hits">
						39					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/ddr3">
							DDR3</a>

											</td>

					
					
										<td class="list-hits">
						43					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/degauss">
							Degauss</a>

											</td>

					
					
										<td class="list-hits">
						38					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/desktop-computer">
							Desktop Computer</a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/digital-camera">
							Digital Camera </a>

											</td>

					
					
										<td class="list-hits">
						37					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/dimm">
							DIMM</a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/disk-drive">
							Disk Drive</a>

											</td>

					
					
										<td class="list-hits">
						41					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/dma">
							DMA</a>

											</td>

					
					
										<td class="list-hits">
						36					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/dongle">
							Dongle</a>

											</td>

					
					
										<td class="list-hits">
						39					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/dot-pitch">
							Dot Pitch</a>

											</td>

					
					
										<td class="list-hits">
						31					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/dram">
							DRAM</a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/dsl">
							DSL</a>

											</td>

					
					
										<td class="list-hits">
						32					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/dslam">
							DSLAM</a>

											</td>

					
					
										<td class="list-hits">
						32					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/dual-core">
							Dual-Core</a>

											</td>

					
					
										<td class="list-hits">
						38					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/dvd">
							DVD</a>

											</td>

					
					
										<td class="list-hits">
						28					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/dvdr">
							DVD±R</a>

											</td>

					
					
										<td class="list-hits">
						39					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/dvdrw">
							DVD±RW</a>

											</td>

					
					
										<td class="list-hits">
						38					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/dvi">
							DVI</a>

											</td>

					
					
										<td class="list-hits">
						32					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/dvr">
							DVR</a>

											</td>

					
					
										<td class="list-hits">
						29					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/ecc">
							ECC</a>

											</td>

					
					
										<td class="list-hits">
						32					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/eide">
							EIDE</a>

											</td>

					
					
										<td class="list-hits">
						31					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/ethernet">
							Ethernet</a>

											</td>

					
					
										<td class="list-hits">
						36					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/expansion-card">
							Expansion Card</a>

											</td>

					
					
										<td class="list-hits">
						40					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/external-hard-drive">
							External Hard Drive</a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/fiber-optic-cable">
							Fiber-Optic Cable</a>

											</td>

					
					
										<td class="list-hits">
						32					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/file-server">
							File Server</a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/firewire">
							Firewire</a>

											</td>

					
					
										<td class="list-hits">
						39					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/flash-drive">
							Flash Drive</a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/flash-memory">
							Flash Memory</a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/flatbed">
							Flatbed</a>

											</td>

					
					
										<td class="list-hits">
						34					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/floppy-disk">
							Floppy Disk</a>

											</td>

					
					
										<td class="list-hits">
						41					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/fpu">
							FPU</a>

											</td>

					
					
										<td class="list-hits">
						27					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/friendly-url">
							Friendly URL</a>

											</td>

					
					
										<td class="list-hits">
						24					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/fsb">
							FSB</a>

											</td>

					
					
										<td class="list-hits">
						33					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/gpu">
							GPU</a>

											</td>

					
					
										<td class="list-hits">
						34					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/hard-disk">
							Hard Disk</a>

											</td>

					
					
										<td class="list-hits">
						45					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/hard-drive">
							Hard Drive</a>

											</td>

					
					
										<td class="list-hits">
						28					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/hard-token">
							Hard Token</a>

											</td>

					
					
										<td class="list-hits">
						43					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/hardware">
							Hardware</a>

											</td>

					
					
										<td class="list-hits">
						31					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/hdmi">
							HDMI</a>

											</td>

					
					
										<td class="list-hits">
						32					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/heat-sink">
							Heat Sink</a>

											</td>

					
					
										<td class="list-hits">
						29					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/host">
							Host</a>

											</td>

					
					
										<td class="list-hits">
						38					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/hsf">
							HSF</a>

											</td>

					
					
										<td class="list-hits">
						37					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/hub">
							Hub</a>

											</td>

					
					
										<td class="list-hits">
						37					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/i/o-address">
							I/O Address</a>

											</td>

					
					
										<td class="list-hits">
						25					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/ibm-compatible">
							IBM Compatible</a>

											</td>

					
					
										<td class="list-hits">
						34					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/ide">
							IDE</a>

											</td>

					
					
										<td class="list-hits">
						29					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/igp">
							IGP</a>

											</td>

					
					
										<td class="list-hits">
						39					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/imap">
							IMAP</a>

											</td>

					
					
										<td class="list-hits">
						46					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/impact-printer">
							Impact Printer</a>

											</td>

					
					
										<td class="list-hits">
						3					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/inkjet">
							Inkjet</a>

											</td>

					
					
										<td class="list-hits">
						3					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/input-device">
							Input Device</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/integrated-circuit">
							Integrated Circuit</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/interface">
							Interface</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/internal-hard-drive">
							Internal Hard Drive</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/ipad">
							iPad</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/iphone">
							iPhone</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/ipod">
							iPod</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/irq">
							IRQ</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/isa">
							ISA</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/joystick">
							Joystick</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/jumper">
							Jumper</a>

											</td>

					
					
										<td class="list-hits">
						3					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/keyboard">
							Keyboard</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/kvm-switch">
							KVM Switch</a>

											</td>

					
					
										<td class="list-hits">
						2					</td>
					
								</tr>
												<tr class="cat-list-row0" >
							
					<td class="list-title">
						<a href="/hardware/laptop">
							Laptop</a>

											</td>

					
					
										<td class="list-hits">
						3					</td>
					
								</tr>
												<tr class="cat-list-row1" >
							
					<td class="list-title">
						<a href="/hardware/laser-printer">
							Laser Printer</a>

											</td>

					
					
										<td class="list-hits">
						5					</td>
					
								</tr>
							</tbody>
	</table>

	
	<div>
		<!-- @TODO add hidden inputs -->
		<input type="hidden" name="filter_order" value="" />
		<input type="hidden" name="filter_order_Dir" value="" />
		<input type="hidden" name="limitstart" value="" />
	</div>
</form>
	</div>

	</div>

<div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="104" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:51:"hardware - টেকনোলজি বেসিক";s:11:"description";s:8:"hardware";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:8:"hardware";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:51:"hardware - টেকনোলজি বেসিক";s:6:"author";N;}}s:5:"links";a:3:{i:0;s:106:"<link href="/hardware?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0"";i:1;s:109:"<link href="/hardware?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0"";i:2;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:1:{s:15:"text/javascript";s:343:"
		window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}