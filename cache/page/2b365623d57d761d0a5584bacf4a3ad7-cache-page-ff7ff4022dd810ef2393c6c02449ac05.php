<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11406:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="ডাটা, করা, থাকে।, বাফারিং, হয়ে, করার, করে, হয়।, জন্য, ব্যবহার, ডিস্কে, পরিবর্তন, অনেক, ডকুমেন্ট, বাফারে" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Buffer - technical - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Buffer - technical বাফার হচ্ছে মেমরির একটা অঞ্চল যা সাময়িকভাবে ডাটা সংরক্ষণের জন্য ব্যবহার করা হয়। ডাট" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Buffer - technical - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106" class="current active"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
		<h2>
										Buffer						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by মাসপি				</dd>
	
		<dd class="hits">
		Hits: 1		</dd>
 </dl>

	
	<p>বাফার হচ্ছে মেমরির  একটা অঞ্চল যা সাময়িকভাবে ডাটা সংরক্ষণের জন্য  ব্যবহার করা হয়। ডাটা  একস্থান থেকে অন্যস্থানে নেয়ার সময় এটা কাজ করে।  একটি ডিস্ক থেকে ডাটা রিড  এবং রাইট করার প্রক্রিয়া ধীর গতিতে থাকে। তাই  অনেক প্রোগ্রাম ফাইলের  পরিবর্তন বাফারে সংরক্ষন করে থাকে, তারপর বাফারের  সংরক্ষনকৃত ডাটা ডিস্কে  প্রেরন করে। যেমন- আপনি যদি মাইক্রোসফট ওয়ার্ডের  কোন ফাইলের ডাটা পরিবর্তন  করেন তাহলে পরিবর্তনটুকু বাফারিং হয়ে সাময়িক  সময়ের জন্য সংরক্ষিত হয়ে থাকে।  যখন আপনি ফাইল সেভ করেন তখন বাফারের ডাটা  ডিস্কে প্রেরন করা হয়। প্রতিবার  ফাইলের ডাটা পরিবর্তন করে ডিস্কে রাইট  করার চেয়ে বাফারিং প্রক্রিয়া অনেক  কার্যকর।</p>
<p><strong> </strong></p>
<p>বাফার শব্দটি সিডি বার্নের ক্ষেত্রে অনেক ব্যবহার করা হয়ে  থাকে।  সেক্ষেত্রে সিডিতে  বার্ণের পূর্বে ডাটা বাফারে সংরক্ষিত হয়ে থাকে।  আবার  ডকুমেন্ট প্রিন্ট করার ক্ষেত্রেও বাফারিং হয়ে থাকে।  প্রিন্ট কমান্ড  দেয়ার  পর অপারেটিং সিস্টেম প্রিন্টের জন্য ডকুমেন্ট বাফারে কপি করে, তারপর   প্রিন্টার ডকুমেন্ট অনুসারে বর্ণ প্রিন্ট করতে থাকে। প্রিন্টিং বাফারিং   করার কারনে কম্পিউটারে প্রিন্টের সাথে সাথে অন্য কাজও স্বাভাবিকভাবে করা   যায় । প্রিন্টিং বাফারিংকে স্পুলিং বলা হয়।</p>
<p>বেশিরভাগ কীবোর্ড ড্রাইভার  বাফারও সংরক্ষণ করে থাকে, যাতে কীবোর্ডে  টাইপ করা ডাটাতে ভুল হলে আবার  পূর্বের ডাটাতে ফিরে যাওয়া যায়। বেশিরভাগ  অপারেটিং সিস্টেমই ডিস্ক হতে রিড  করা ডাটা বাফারিং করে থাকে । যাতে তা খুব  সহজেই পরবর্তী কোন কাজের জন্য  ব্যবহার করা হয়।</p>
<p>অনলাইনে ভিডিও দেখার সময় বাফারিং করা হয়ে থাকে। যেমন -  আমরা যখন  ইউটিউবে ভিডিও দেখি, পুরো ভিডিও ডাউনলোড করার আগেই যতটুকু ডাউনলোড  হয়েছে  তা সহজেই দেখতে পাই। বাফারিং প্রক্রিয়ার কারনেই এটা সম্ভব হয়েছে।</p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/technical/baseband" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/technical/burn" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/technical/buffer" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/technical/buffer" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="106" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:61:"Buffer - technical - টেকনোলজি বেসিক";s:11:"description";s:242:"Buffer - technical বাফার হচ্ছে মেমরির একটা অঞ্চল যা সাময়িকভাবে ডাটা সংরক্ষণের জন্য ব্যবহার করা হয়। ডাট";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:253:"ডাটা, করা, থাকে।, বাফারিং, হয়ে, করার, করে, হয়।, জন্য, ব্যবহার, ডিস্কে, পরিবর্তন, অনেক, ডকুমেন্ট, বাফারে";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:61:"Buffer - technical - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}