<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12238:"<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
<head>
	  <base href="http://www.technologybasic.com/" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="robots" content="index, follow" />
  <meta name="keywords" content="করে, ঐ, নেটওয়ার্কের, প্রক্সি, কোন, করতে, ক্যাশ, তথ্য, বিভিন্ন, ভিজিট, একটি, ফলে, ব্যবহার, সার্ভার, মধ্যে" />
  <meta name="rights" content="Department of Computer Science &amp; Telecommunication Engineering, Noakhali Science and Technology University (NSTU) " />
  <meta name="language" content="en-GB" />
  <meta name="title" content="Proxy Server - internet - টেকনোলজি বেসিক" />
  <meta name="author" content="Super User" />
  <meta name="description" content="Proxy Server - internet বড় বড় ব্যবসা প্রতিষ্ঠান, সংস্থা এবং বিশ্ববিদ্যালয়গুলো প্রক্সি সার্ভার ব্যবহার করে থাকে। এটা এমন একটি সার্ভার যেখানে ইন্টারনেটের মধ্য দিয়ে সকল কম্পিউটারে তথ্য যাওয়ার সময় লোকাল নেটওয়ার্কের মধ্য দিয়ে যেতে হয়। প্রক্সি সার্ভার ব্যবহারের" />
  <meta name="generator" content="Joomla! 1.6 - Open Source Content Management" />
  <title>Proxy Server - internet - টেকনোলজি বেসিক</title>
  <link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="/media/system/js/core.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-core.js"></script>
  <script type="text/javascript" src="/media/system/js/caption.js"></script>
  <script type="text/javascript" src="/media/system/js/mootools-more.js"></script>

	<!-- The following line loads the template CSS file located in the template folder. -->
	<link rel="stylesheet" href="/templates/siteground-j16-12/css/template.css" type="text/css" />
	
	<!-- The following line loads the template JavaScript file located in the template folder. It's blank by default. -->
	<script type="text/javascript" src="/templates/siteground-j16-12/js/CreateHTML5Elements.js"></script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript">jQuery.noConflict();</script>
	<script type="text/javascript" src="/templates/siteground-j16-12/js/sgmenu.js"></script>


</head>
<body class="page_bg">
	<div class="wrapper">
	<header>
		<table cellpadding="0" cellspacing="0">
			<tr><td>
				<h1><a href=""></a></h1>
			</td></tr>
		</table>		
		
		<div class="top-menu">
			<div id="sgmenu">
				
			</div>
		</div>
	</header>
	
	<section id="content">
				<div class="maincol">			 	
				
					<div class="leftcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Basic Terms</h3>
											

<ul class="menu">
<li id="item-101" class="current"><a href="/internet" ><span>ইন্টারনেট টার্ম</span></a></li><li id="item-104"><a href="/hardware" ><span>হার্ডওয়ার টার্ম</span></a></li><li id="item-147"><a href="/software" ><span>সফটওয়্যার টার্ম</span></a></li><li id="item-106"><a href="/technical" ><span>টেকনিক্যাল টার্ম</span></a></li><li id="item-107"><a href="/fileformat" ><span>ফাইল ফরমেট টার্ম</span></a></li><li id="item-108"><a href="/bits" ><span>বিটস এবং বাইট টার্ম</span></a></li><li id="item-109"><a href="/acronyms" ><span>টেক আক্রোনিমস</span></a></li></ul>					</div>
				</div>
			</div>
		</div>
	
			</div>
						
				<div class="cont">
				
									
					<div class="item-page">
<h1>
	Articles</h1>
		<h2>
										Proxy Server						</h2>



	
	

 <dl class="article-info">
 <dt class="article-info-term">Details</dt>
	<dd class="createdby"> 
				
							Written by মাসপি				</dd>
	
		<dd class="hits">
		Hits: 28		</dd>
 </dl>

	
	<p>বড় বড় ব্যবসা  প্রতিষ্ঠান, সংস্থা এবং বিশ্ববিদ্যালয়গুলো প্রক্সি  সার্ভার ব্যবহার করে  থাকে। এটা এমন একটি সার্ভার যেখানে ইন্টারনেটের মধ্য  দিয়ে সকল কম্পিউটারে  তথ্য যাওয়ার সময় লোকাল নেটওয়ার্কের মধ্য দিয়ে যেতে  হয়। প্রক্সি সার্ভার  ব্যবহারের মাধ্যমে একটি সংস্থা তাদের নেটওয়ার্কের  উন্নতি সাধন করতে পারে  এবং ব্যবহারকারীদের বিভিন্ন তথ্য পাওয়ার উপর  নিয়ন্ত্রন নিতে পারে।  ক্যাশিং পদ্ধতি ব্যবহার করে প্রক্সি সার্ভারগুলো  ইন্টারনেটে প্রবেশের  ক্ষেত্রে তাদের নেটওয়ার্কের গতি বাড়ায়। ক্যাশ সিস্টেম  বর্তমানে দেখা  ওয়েবসাইট, ছবি, বিভিন্ন ফাইল হার্ডডিস্কে সেভ করে রাখে,  ফলে ঐ ওয়েবসাইটে  পুণরায় ভিজিট করলে এসব ফাইল আবার ডাউনলোড করতে হবে না।   এর মানে আপনি যদি  কোন একটি ওয়েবপেজ একবার ভিজিট করেন তাহলে ঐ পেজের  বিভিন্ন কনটেন্ট  হার্ডডিস্কে সেভ করে রাখার ফলে ঐ সাইটটি আরেকবার ভিজিট  করলে অনেক তাড়াতাড়ি  আপনি প্রবেশ করতে পারবেন।</p>
<p><strong> </strong> যেমনভাবে ওয়েব ব্রাউজার আপনার বর্তমানে দেখা   ফাইলগুলো ক্যাশ করে রাখে ঠিক তেমনি প্রক্সি সার্ভারও নেটওয়ার্কের মধ্যে আসা   তথ্যগুলো ক্যাশ করে রাখে। ফলে কোন ব্যক্তি যদি কোন নির্দিষ্ট ওয়েবসাইটে   ১.oo সময়ে প্রবেশ করে এবং একই নেটওয়ার্কে অন্তর্ভুক্ত অপর ব্যাক্তি ঐ সাইটে   কাছাকাছি সময়ে অনেক দ্রুত প্রবেশ করতে পারবে। কারণ তখন সরাসরি ঐ  ওয়েবপেজের  কন্টেন্টগুলো প্রক্সি সার্ভারের ক্যাশ থেকে অপরব্যাক্তির কাছে  যায়। এর  মানে ভিজিটর ঐ ওয়েবপেজ তাড়াতাড়ি দেখতে পারবে কিন্তু ঐ ওয়েবপেজের  আপডেটগুলো  তাড়াতাড়ি দেখতে পারবে না।  এছাড়া প্রক্সিসার্ভারের আরেকটি বড়   উদ্দেশ্য হল- নেটওয়ার্কের মধ্যে ব্যভারকারীর তথ্য নিয়ন্ত্রন করা। যেমন- কোন   প্রতিষ্ঠানের কর্মীরা যেন সামাজিক যোগাযোগের সাইটগুলো ব্যবহার করে কাজের   সময় নষ্ট না করে তাই এসব ওয়েবসাইটগুলো ঐ নেটওয়ার্কের মধ্যে বন্ধ করে দেয়া   হয়। আবার প্রক্সি সার্ভারগুলো HTTP,FTP,HTTPS বিভিন্ন প্রটোকল ফ্লিটার করে   থাকে।</p>
<p><img src="images/internet/PT/proxy-server.jpg" border="0" /></p> 
				<ul class="pagenav">
					<li class="pagenav-prev">
						<a href="/internet/radcab" rel="next">&lt; Prev</a>
					</li>
					<li class="pagenav-next">
						<a href="/internet/protocol" rel="prev">Next &gt;</a>
					</li>
				</ul>
	<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like action="like" colorscheme="light" layout="standard" href="http://www.technologybasic.com/internet/proxy-server" send="true" show_faces="true" font=""></fb:like><fb:comments href="http://www.technologybasic.com/internet/proxy-server" send_notification_uid="100002524152090" num_posts="5" width="500" colorscheme="light"></fb:comments></div><div style="text-align:center; font-size: 9px; visibility: visible;" title="Joomla SEO by AceSEF"><a href="http://www.joomace.net/joomla-extensions/acesef" target="_blank">Joomla SEO by AceSEF</a></div>
				</div>
			
					<div class="rightcol">
						<div class="module">
			<div>
				<div>
					<div>
													<h3>Bangla Problem?</h3>
											<div class="custom">
	<p><a href="http://technologybasic.com/bangla">can't see Bangla?</a></p>
<p><a href="http://technologybasic.com/bangla"></a><a href="http://technologybasic.com/bangla"><strong>click here .</strong></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Thanks To</h3>
											<div class="custom">
	<p><a href="http://www.linuxpathshala.com" target="_blank" title=" লিনাক্স পাঠশালা"> </a><a href="http://www.linuxpathshala.com" target="_blank" title="লিনাক্স পাঠশালা"><img src="images/linux.gif" border="auto" width="146" height="38" style="vertical-align: middle;" /></a></p></div>					</div>
				</div>
			</div>
		</div>
			<div class="module">
			<div>
				<div>
					<div>
													<h3>Search</h3>
											<form action="/index.php" method="post">
	<div class="search">
		<label for="mod-search-searchword">Search...</label><input name="searchword" id="mod-search-searchword" maxlength="20"  class="inputbox" type="text" size="20" value="         Search Terms"  onblur="if (this.value=='') this.value='         Search Terms';" onfocus="if (this.value=='         Search Terms') this.value='';" /><input type="image" value="write here something" class="button" src="" onclick="this.form.searchword.focus();"/>	<input type="hidden" name="task" value="search" />
	<input type="hidden" name="option" value="com_search" />
	<input type="hidden" name="Itemid" value="0" />
	</div>
</form>
					</div>
				</div>
			</div>
		</div>
	
			</div>
				<div class="clr"></div>
		</div>
	</section>
	
	
	<footer>
	<p style="text-align:center;"><!-- SIDE BEGIN --><!-- SIDE END -->
<p>টেকনোলজি বেসিক, <span style="font-size: xx-small;">Created by</span> <span style="font-size: medium;"><span style="color: #ff0000;"><a class="sgfooter" href="http://www.nstu.edu.bd/cste/" target="_blank">Department of Computer Science &amp; Telecommunication Engineering,</a></span> <span style="color: #000000;"><a class="sgfooter" href="http://www.nstu.edu.bd/" target="_blank">Noakhali Science and Technology University (NSTU)</a></span>&nbsp;</span></p></p>
	</footer>
	</div>
</body>
</html>";s:4:"head";a:10:{s:5:"title";s:66:"Proxy Server - internet - টেকনোলজি বেসিক";s:11:"description";s:649:"Proxy Server - internet বড় বড় ব্যবসা প্রতিষ্ঠান, সংস্থা এবং বিশ্ববিদ্যালয়গুলো প্রক্সি সার্ভার ব্যবহার করে থাকে। এটা এমন একটি সার্ভার যেখানে ইন্টারনেটের মধ্য দিয়ে সকল কম্পিউটারে তথ্য যাওয়ার সময় লোকাল নেটওয়ার্কের মধ্য দিয়ে যেতে হয়। প্রক্সি সার্ভার ব্যবহারের";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:6:{s:6:"robots";s:13:"index, follow";s:8:"keywords";s:256:"করে, ঐ, নেটওয়ার্কের, প্রক্সি, কোন, করতে, ক্যাশ, তথ্য, বিভিন্ন, ভিজিট, একটি, ফলে, ব্যবহার, সার্ভার, মধ্যে";s:6:"rights";s:114:"Department of Computer Science & Telecommunication Engineering, Noakhali Science and Technology University (NSTU) ";s:8:"language";s:5:"en-GB";s:5:"title";s:66:"Proxy Server - internet - টেকনোলজি বেসিক";s:6:"author";s:10:"Super User";}}s:5:"links";a:1:{i:0;s:105:"<link href="/templates/siteground-j16-12/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon"";}s:11:"styleSheets";a:0:{}s:5:"style";a:0:{}s:7:"scripts";a:4:{s:24:"/media/system/js/core.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-core.js";s:15:"text/javascript";s:27:"/media/system/js/caption.js";s:15:"text/javascript";s:33:"/media/system/js/mootools-more.js";s:15:"text/javascript";}s:6:"script";a:0:{}s:6:"custom";a:0:{}}s:7:"pathway";s:1:"<";s:6:"module";a:0:{}}