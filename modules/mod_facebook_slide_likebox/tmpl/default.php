<?php 
/**
 * Facebook Slide FanBox
 * @license    GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @link       http://facebooklikebox.net
 */
 
	// no direct access
	defined( '_JEXEC' ) or die( 'Restricted access' );	
	$document = & JFactory::getDocument();
	if ($params->get('position') == 1) {
		$document->addStyleSheet(JURI::root() . 'modules/mod_facebook_slide_likebox/tmpl/css/style2'.'.css', 'text/css', null, array() ); }
	else if ($params->get('position') == 0){
		$document->addStyleSheet(JURI::root() . 'modules/mod_facebook_slide_likebox/tmpl/css/style1'.'.css', 'text/css', null, array() ); }
	echo $slidelikebox;
?>