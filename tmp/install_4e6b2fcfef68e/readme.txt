Joomtrax blink Plugin.
Version: 1.6
Date: 10 June 2011
---------------------------------------
Welcome, 
this is The Free version of the blink Plugin.
If you like this plugin, please support us,
by making a donation, or buy an other plugin at
www.joomtrax.com


There are two zip files in this folder:
-Joomla15blinkPlugin.zip    -> This is the joomla� 1.5 version.
-Joomla16blinkPlugin.zip    -> This is the joomla� 1.6 version. 

Make sure you install the correct plugin version of the associated joomla� version.

Don't forget to publish the plugin in the plugin manager.

In your articles type or copy: {blink}Hello World!{/blink}

--------------------------
www.Joomtrax.com

-----------------------------------------------------------------------------------

Version history:
-----
version: 1.7  date: 9 September 2011
Added Joomla 1.7 version
-----
version: 1.6  date: 10 June 2011
Fixed blink span tag instead of div tag
-----
version: 1.5  date: 3 June 2011
Fixed blink only one article in blog bug
-----
version: 1.4  date: 19 may 2011
Fixed joomla 1.6 page title bug
-----
version: 1.3  date: 12 march 2011
Added support setting of system interval 
refactored to Joomla! and other jvascript, html standards
added support of blink in multiple articles 
-----
version: 1.02  date: 7 march 2011
Added support of interval blink
-----
version: 1.01  date: 2 march 2011
-----
version: 1.00  date: 27 february 2011
Initial Version
-----

-----------------------------------------------------------------------------------
The Blink plugin javascript is fully under GPL License, also the javascript code.

Joomtrax Licensing:

This End User License Agreement is a binding legal agreement between you and Joomtrax.

Purchase, installation or use of Joomtrax Extensions provided on www.joomtrax.com signifies that you have read, understood, and agreed to be bound by the terms outlined below.

JOOMTRAX GPL LICENSING

All Joomla Extensions produced by Joomtrax. are released under the GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html). Specifically, the PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript , Silverlight and Flash files are NOT GPL, and are released under the Joomtrax Proprietary Use License v1.0 (See below) unless specifically authorized by Joomtrax. Elements of the extensions released under this proprietary license may not be redistributed or repackaged for use other than those allowed by the Terms of Service.


JOOMTRAX PROPRIETARY USE LICENSE (v1.0)

The JOOMTRAX Proprietary Use License covers any images, cascading stylesheets, manuals and JavaScript, Silverlight and Flash files in any extensions produced and/or distributed by Joomtrax.com. These files are copyrighted by Joomtrax.com and cannot be redistributed in any form without prior consent from Joomtrax.com

The Blink plugin javascript is fully under GPL License, also the javascript code.

USAGE TERMS

You are allowed to use the extensions on more than one domain. We do not provide any support to any Joomtrax product.
You are allowed to make any changes to the code, however modified code will not be supported by us.


MODIFICATION OF EXTENSIONS PRODUCED BY JOOMTRAX.COM.

You are authorized to make any modification(s) to joomtrax extension PHP code. However no support will be available to you.

In accordance with the Joomtrax Proprietary Use License v1.0, you may not release any proprietary files (modified or otherwise) under the GPL license. The terms of this license and the GPL v2 prohibit the removal of the copyright information from any file.

Please contact us if you have any requirements that are not covered by these terms.

--------------------------

Joomtrax Disclaimer:

   1. Joomtrax is not responsible for any damage or lost of data by using joomtrax products.
   2. Joomtrax has the right to cancel the support of the joomtrax products at any time.
   3. Joomtrax products are as is.
   4. Joomtrax has the right to stop the joomtrax site at any time.

--------------------------

http://www.joomtrax.com is not affiliated with or endorsed by the Joomla! Project or Open Source Matters.
The Joomla! name and logo is used under a limited license granted by Open Source Matters the trademark holder in the United States and other countries.